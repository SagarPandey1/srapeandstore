const express = require("express"); 
const app  = express();
app.use(express.static("./public"));


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });  


const dashRoutes=require("./routes/dashboard/dash.js")
app.use('/dash',dashRoutes)


const basic =require("./routes/ArticlesAndTwitter/Articles") 
app.use('/',basic)

const filterPageRoutes = require("./routes/ArticlesAndTwitter/filtersPageRoutes")
app.use('/filters',filterPageRoutes)

app.listen(2222,()=>{console.log("Server start.....")})