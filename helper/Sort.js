var operations = {


    sortByMaxLikes(object) {

        //console.log(object[0].WebInfo.urlsInfo.fbStats.TotalLikes);
        var sortedArray = object.sort(function (a, b) {
            if (a.WebInfo.urlsInfo.fbStats == undefined) {
                return;
            }
            if (b.WebInfo.urlsInfo.fbStats == undefined) {
                return;
            }
            return b.WebInfo.urlsInfo.fbStats.TotalLikes - a.WebInfo.urlsInfo.fbStats.TotalLikes;
        })

        return sortedArray.slice(0, 20);
    },

    sortByMaxTfIdf(termsArray, object) {

        result = object
        termsList = termsArray
        New_Array_List = []
        for (var m = 0; m < result.length; m++) {
            matchedElem = 0
            matchedArrayList = []

            for (var i = 0; i < result[m].WebInfo.urlsInfo.tfidf.length; i++) {

                for (var j = 0; j < termsList.length; j++) {

                    if (result[m].WebInfo.urlsInfo.tfidf[i].TermsName == termsList[j].TermsName) {
                        matchedElem++;

                        matchedArrayList.push(result[m].WebInfo.urlsInfo.tfidf[i])
                    }

                }
            }
            if (result[m].WebInfo.urlsInfo.fbStats == undefined ||
                           result[m].WebInfo.urlsInfo.Sentiments == undefined) {
                New_Array_List.push({
                    "WebSite": result[m].WebSite,
                    "Title": result[m].WebInfo.urlsInfo.Title,
                    "Url": result[m].WebInfo.urlsInfo.urlName,
                    "MatchedItem": matchedElem,
                    "MatchedTerms": matchedArrayList,
                    "image": result[m].WebInfo.urlsInfo.ImageSource,
                    "BaseUrl": result[m].WebInfo.urlsInfo.BaseUrl,
                    "Tags": result[m].WebInfo.urlsInfo.Tags,
                    "Date": result[m].WebInfo.urlsInfo.Date,
                    "Sentiments": "",
                    "TotalLikes": "",
                    "TotalComments": ""
                })
            } else {
                New_Array_List.push({
                    "WebSite": result[m].WebSite,
                    "Title": result[m].WebInfo.urlsInfo.Title,
                    "Url": result[m].WebInfo.urlsInfo.urlName,
                    "MatchedItem": matchedElem,
                    "MatchedTerms": matchedArrayList,
                    "image": result[m].WebInfo.urlsInfo.ImageSource,
                    "BaseUrl": result[m].WebInfo.urlsInfo.BaseUrl,
                    "Tags": result[m].WebInfo.urlsInfo.Tags,
                    "Date": result[m].WebInfo.urlsInfo.Date,
                    "Sentiments": result[m].WebInfo.urlsInfo.Sentiments.Article.compound,
                    "TotalLikes": result[m].WebInfo.urlsInfo.fbStats.TotalLikes,
                    "TotalComments": result[m].WebInfo.urlsInfo.fbStats.TotalComments
                })
            }

        }
        return New_Array_List.slice(0, 20)
    }

}

module.exports = operations