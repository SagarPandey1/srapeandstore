var articlesUrl={
    
    ArticlesJSON : "http://139.59.11.40:2222/getArticles",
    RelatedArticlesJSON : "http://139.59.11.40:2222/getRelatedArticles",
    TotalResultJSON : "http://139.59.11.40:2222/getNoOfResults",
    DetailsJSON : "http://139.59.11.40:2222/getFullDetails",
     
}

var indexUrl ={  
    TotalDataJSON : "http://139.59.11.40:2222/dash/totaldataInfo",
    TotalUrlsJSON : "http://139.59.11.40:2222/dash/totalUrls" ,
    TotalArticlesJSON : "http://139.59.11.40:2222/dash/totalArticles",
    TotalNounsJSON : "http://139.59.11.40:2222/dash/totalNouns",
    TotalSentimentsJSON : "http://139.59.11.40:2222/dash/totalSentiments",
    TotalHandlesJSON : "http://139.59.11.40:2222/dash/totalHandles",
    TotalTweetsJSON : "http://139.59.11.40:2222/dash/totalTweets",
        
}

var indianKanoonUrl ={ 

     KanoonDataJSON : "http://139.59.11.40:2222/indianKanoon",
     KanoonDetailsJSON : "http://139.59.11.40:2222/indianKanoonDump",
    RelatedCasesJSON : "http://139.59.11.40:2222/RelatedCases",
    TotalCasesResultJSON : "http://139.59.11.40:2222/totalcases",

}

var socialHandlesUrl = {
    
     AllSocialHandlesJSON :  "http://139.59.11.40:2222/getAllSocialHandles",
     StoreSocialHandle : "http://139.59.11.40:2222/getSocialHandle",    
    
}



var tweetDataJSON = {
    
    AllTweetDataJSON : "http://139.59.11.40:2222/getTwitterData", 
    NoofTweetsJSON : "http://139.59.11.40:2222/getNoOfTweets",
    DetailsJSON : "http://139.59.11.40:2222/getFullTweetDetails"
    
}


var filtersUrl = {
    
    filterByDate : "http://139.59.11.40:2222/filters/Date",
    TotalArticlesByDateJSON : "http://139.59.11.40:2222/filters/totalArticlesByDate"
}  

var sortArticlesUrl ={
    
    sortByDate : "http://139.59.11.40:2222/articlesSortByDate",
    sortByPopularity : "http://139.59.11.40:2222/articlesSortByPopularity"
}