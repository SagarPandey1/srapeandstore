app.constant("HOME", "/");
app.constant("ARTICLES", "/articles");
app.constant("INDIANKANOON", "/indianKanoon");
app.constant("SOCIALMEDIAINFO", "/SocialMediaInfo");
app.constant("URLS", "/urlsPage");
app.constant("TWEETS", "/tweetPage");

/*-----app.constant("FILTER", "/filterPage");  */


/////////////////// DASHBOARD PAGES URL ROUTING INFORMATION //////////////////////////


app.constant("DASH_URL", "/dash/urlsdetails");
app.constant("DASH_ARTICLES", "/dash/articlesdetails");
app.constant("DASH_SENTIMENTS", "/dash/sentimentsdetails");
app.constant("DASH_NOUNS", "/dash/nounsdetails");
app.constant("DASH_HANDLES", "/dash/handlesdetails");
app.constant("DASH_TWEETS", "/dash/tweetdetails");

/////////////////// ARTICLES PAGE ROUTING INFORMATION  ////////////////////////

app.constant("SORT_BY_DATE","/articles/sortbyDate")
app.constant("SORT_BY_POPULARITY","/articles/sortbyPopularity")
