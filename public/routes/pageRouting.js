app.config(function ($routeProvider, $locationProvider, HOME, ARTICLES, INDIANKANOON, SOCIALMEDIAINFO, URLS, TWEETS ) {
    $locationProvider.hashPrefix('');
    $routeProvider.when(HOME, {
        templateUrl: "../views/home.html",
        controller: "indexctrl"
    }).when(ARTICLES, {
        templateUrl: "../views/articles.html",
        controller: "articlesCtrl"
    }).when(INDIANKANOON, {
        templateUrl: "../views/indianKanoon.html",
        controller: "kanoonCtrl"
    }).when(SOCIALMEDIAINFO, {
        templateUrl: "../views/SocialMediaInfo.html",
        controller: "socialmediaCtrl"
    }).when(URLS, {
        templateUrl: "../views/urlspage.html",
        controller: "urlsCtrl"
    }).when(TWEETS, {
        templateUrl: "../views/tweetPage.html",
        controller: "twitterCtrl"
    }).otherwise({
        template: "Error Page , No Match Found",
        redirectTo: "/"
    });
});
