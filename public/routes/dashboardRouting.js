app.config(function ($routeProvider, $locationProvider,DASH_URL,DASH_ARTICLES,DASH_SENTIMENTS,DASH_NOUNS,DASH_HANDLES,DASH_TWEETS) {
    $locationProvider.hashPrefix('');
    $routeProvider.when(DASH_URL, {
        templateUrl: "../views/DashViews/urlsDetails.html",
        controller: "urlsDetailsCtrl"
    }).when(DASH_ARTICLES, {
        templateUrl: "../views/DashViews/articlesDetails.html",
        controller: "ArticlesDetailsCtrl"
    }).when(DASH_SENTIMENTS, {
        templateUrl: "../views/DashViews/sentimentsDetails.html",
        controller: "sentimentsDetailsCtrl"
    }).when(DASH_NOUNS, {
        templateUrl: "../views/DashViews/nounsDetails.html",
        controller: "nounsDetailsCtrl"
    }).when(DASH_HANDLES, {
        templateUrl: "../views/DashViews/handlesDetails.html",
        controller: "handlesDetailsCtrl"
    }).when(DASH_TWEETS, {
        templateUrl: "../views/DashViews/tweetsDetails.html",
        controller: "tweetsDetailsCtrl"
    }).otherwise({
        template: "Error Page , No Match Found",
        redirectTo: "/"
    });
});
