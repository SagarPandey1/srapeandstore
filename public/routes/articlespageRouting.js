app.config(function ($routeProvider, $locationProvider, SORT_BY_DATE,SORT_BY_POPULARITY) {
    $locationProvider.hashPrefix('');
    $routeProvider.when(SORT_BY_DATE, {
        templateUrl: "../views/articles.html",
        controller: "sortByDatectrl"
    }).when(SORT_BY_POPULARITY, {
        templateUrl: "../views/articles.html",
        controller: "sortByPopularityCtrl"
    }).otherwise({
        template: "Error Page , No Match Found",
        redirectTo: "/"
    });
});
