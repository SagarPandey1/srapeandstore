app.controller("indexctrl", function ($scope, $rootScope) {
    $rootScope.HeadersName = "DashBoard"
    $scope.dashmain = false;
    $scope.loader = true;

    $scope.tfidfCounting = function (obj) {
        var count = 0
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].WebInfo == undefined) {
                continue
            }
            count += obj[i].WebInfo.urlsInfo.tfidf.length
        }

        return count
    }
    

})
