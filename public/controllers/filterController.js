app.controller("filterCtrl", function ($scope, $rootScope, moment, $mdDateLocale, filtersFactory) {
    $rootScope.HeadersName = "Articles Hub";
    $scope.selectedDate = new Date();

    $scope.NUMBER_OF_ITEMS = 10;
    $scope.PAGE_NUMBER = 1;
    $scope.relArticles = false
    $scope.previous = true
    $scope.next = true
    $scope.main = false
    $scope.articleslengthShow = false
    $scope.loader2 = true
    $scope.relLoader = -1;


    $mdDateLocale.formatDate = function (date) {
        return moment(date).format('YYYY-MM-DD');
    };



    $scope.getArticlesOfDate = function () {
            $scope.loader = true
            $scope.main = false

            var date = moment($scope.selectedDate).format('YYYY-MM-DD');
            console.log("Date Selected :- " + date)
            var promise = filtersFactory.getArticlesByDate($scope.PAGE_NUMBER, $scope.NUMBER_OF_ITEMS, date);
            
            promise.then(success, error);
            function success(data) {
                $scope.article = data.data;
                if (data.data.data.docLength == 0) {
                    $scope.loader = false
                    $scope.ServerMsg = true
                    $scope.Message = data.data.data.message
                } else {
                    $scope.noOfArticlesFound(date);
                    $scope.ServerMsg = false;
                    console.log(data.data.data);
                    $scope.next = false;
                    $scope.main = true;
                    $scope.loader = false;
                    $scope.loader2 = false;
                    $scope.articleslengthShow = true;
                    if (data.data.data.length <= 10) {
                        $scope.next = true
                    }
                    $scope.TotalArticlesLength = data.data.data.length;
                }

            }

            function error(er) {
                $scope.ArticleError = er;
                console.log(er);
            }

        },

        $scope.noOfArticlesFound = function (date) {

            var promise = filtersFactory.getNoOfResult(date);
            promise.then(success, error);

            function success(data) {
                $scope.TotalArticlesLength = data.data.data.totalLength
                $scope.TotalPages = Math.trunc($scope.TotalArticlesLength/10+1)
                console.log(data.data.data)
                $scope.articleslengthShow = true;
                $scope.loader2 = false;
            }

            function error(er) {
                $scope.ArticleError = er;
                console.log(er);
            }

        },
        $scope.showFullDetails = function (Object, index) {
            var title = Object.target.value;
            $scope.jsonDataView = !$scope.jsonDataView
            $scope.relArticles = false
            $scope.relLoader = index
            if ($scope.jsonDataView) {
                var promise = filtersFactory.showFullDetails(title);
                promise.then(pass, fail);

                function pass(data) {
                    $scope.relLoader = -1
                    $scope.myJSON = data.data.data
                    $scope.jsonData = index

                }

                function fail(er) {
                    $scope.error = er;
                    console.log(er);
                }
            }
        },
        
        $scope.getRelatedArticles = function (event, index) {
        $scope.relArticles = !$scope.relArticles
        $scope.jsonDataView = false
        $scope.relLoader = index
        var termList = event.target.value;
        if ($scope.relArticles) {
            var promise = filtersFactory.getRelatedArticles(termList);
            promise.then(pass, fail);
            
                function pass(data){
                console.log(data.data.data)   
                $scope.Relatedarticle = data.data.data
                $scope.showTotalRelatedData = true;
                $scope.relLoader = -1
                $scope.showArticles = index

            }

            function fail(er) {
                $scope.error = er;
                console.log(er);
            }
        }
    }

        $scope.NextPage = function () {

            $scope.PAGE_NUMBER += 1
            $scope.previous = false;
            var articlesend = $scope.NUMBER_OF_ITEMS * ($scope.PAGE_NUMBER - 1) + 10
            if (articlesend <= $scope.TotalArticlesLength) {
                $scope.getArticlesOfDate();
            } else {
                $scope.next = true
            }
        },

        $scope.PreviousPage = function () {

            $scope.PAGE_NUMBER -= 1
            var articlesStart = $scope.NUMBER_OF_ITEMS * ($scope.PAGE_NUMBER - 1) + 1
            console.log(articlesStart);
            if (articlesStart <= 10) {
                $scope.previous = true
                $scope.getArticlesOfDate();
            } else {
                $scope.getArticlesOfDate();
                $scope.next = false;
            }
        },
            
      $scope.getPage = function () {
           
           if(parseInt($scope.pageNo) > (parseInt($scope.TotalArticlesLength/10)+1)){
               window.alert("Please enter page number less then or equal to "+(parseInt($scope.TotalArticlesLength/10)+1));
           }
           else if(parseInt($scope.pageNo)==0){
                window.alert("Please enter page number greater then "+0)
           }
        
        else{
            $scope.PAGE_NUMBER = parseInt($scope.pageNo);
            $scope.loader = true
            $scope.main = false
            var date = moment($scope.selectedDate).format('YYYY-MM-DD');
            var promise = filtersFactory.getArticlesByDate($scope.PAGE_NUMBER, $scope.NUMBER_OF_ITEMS,date);
            promise.then(success, error);

            function success(data) {
                $scope.article = data.data;
                console.log(data.data.data)
                $scope.next = false
                $scope.main = true
                $scope.loader = false
                if ($scope.PAGE_NUMBER == 1) {
                    $scope.previous = true;
                } else {
                    $scope.previous = false;
                }
                if($scope.pageNo == (parseInt($scope.TotalArticlesLength/10)+1)){
                    $scope.next = true
                }
            }

            function error(er) {
                $scope.ArticleError = er;
                console.log(er);
            }

        }  
    }        

         


})
