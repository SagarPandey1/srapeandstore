app.controller("urlsCtrl",function($scope,$rootScope,articlesFactory){
    
    $rootScope.HeadersName="Url's List"
    
    $scope.NUMBER_OF_ITEMS = 10;
    $scope.PAGE_NUMBER = 1;
    $scope.relArticles = false
    $scope.previous=true
    $scope.next=true
    $scope.loader=true
    $scope.main=false
    $scope.totalArticlesShow=false
    $scope.loader2=true

    $scope.getArticlesPromise = function () {

            var promise = articlesFactory.getArticles($scope.PAGE_NUMBER, $scope.NUMBER_OF_ITEMS);
            promise.then(success, error);

            function success(data) {
                $scope.article = data.data;
                console.log(data.data.data)
                $scope.next=false
                $scope.main=true
                $scope.loader=false
            }

            function error(er) {
                $scope.ArticleError = er;
                console.log(er);
            }


        },

    $scope.getTotalNoOfArticles = function () {

        var promise = articlesFactory.getNoOfResult();
        promise.then(success, error);


        function success(data) {
            $scope.TotalArticlesLength = data.data.data.length
            console.log(data.data.data)
            $scope.totalArticlesShow=true;
            $scope.loader2=false;
        }

        function error(er) {
            $scope.ArticleError = er;
            console.log(er);
        }

    }


        
    $scope.NextPage=function(){
        
        $scope.PAGE_NUMBER+=1
        $scope.previous=false;
        var articlesend=$scope.NUMBER_OF_ITEMS * ($scope.PAGE_NUMBER - 1)+10
        if(articlesend<=$scope.TotalArticlesLength){
            $scope.getArticlesPromise();
           }
        else{
            $scope.next=true
        }
    }, 
    
    $scope.PreviousPage=function(){
        
        $scope.PAGE_NUMBER-=1
        var articlesStart=$scope.NUMBER_OF_ITEMS * ($scope.PAGE_NUMBER - 1)+1
        console.log(articlesStart)
            if(articlesStart<=10){
                $scope.previous=true
                $scope.getArticlesPromise();
           }
        else{
             $scope.getArticlesPromise();
             $scope.next=false;
        }
        
        
        
    }        


    $scope.getTotalNoOfArticles()
    $scope.getArticlesPromise()





})