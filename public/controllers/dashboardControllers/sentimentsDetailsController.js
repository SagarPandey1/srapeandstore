app.controller("sentimentsDetailsCtrl", function ($scope, $rootScope, indexfactory) {
    
    $rootScope.HeadersName = "DashBoard"
    
    $scope.getTotalSentiments = function () {
            var promise = indexfactory.getTotalSentiments();
            promise.then(pass, fail);

            function pass(data) {
                $scope.TotalSentiments = data.data.data
                console.log(data.data.data)
            }

            function fail(er) {
                $scope.error = er;
                console.log(er)
            }

        }
  
    $scope.getTotalSentiments()
   
})
