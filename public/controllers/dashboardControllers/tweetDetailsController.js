app.controller("tweetsDetailsCtrl", function ($scope, $rootScope, indexfactory) {
    
    $rootScope.HeadersName = "DashBoard"
    
    $scope.getTotalTweets = function () {
            var promise = indexfactory.getTotalTweets();
            promise.then(pass, fail);

            function pass(data) {
                $scope.TotalTweets = data.data.data
                console.log(data.data.data)
            }

            function fail(er) {
                $scope.error = er;
                console.log(er)
            }

        }

    $scope.getTotalTweets()
})
