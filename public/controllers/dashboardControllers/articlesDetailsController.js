app.controller("ArticlesDetailsCtrl", function ($scope, $rootScope, indexfactory) {
    
    $rootScope.HeadersName = "DashBoard"
    
    $scope.getTotalArticles = function () {
            var promise = indexfactory.getTotalArticles();
            promise.then(pass, fail);

            function pass(data) {
                $scope.TotalArticles = data.data.data
                console.log(data.data.data)
            }

            function fail(er) {
                $scope.error = er;
                console.log(er)
            }

        }
    $scope.getTotalArticles()

})
