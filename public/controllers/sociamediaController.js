app.controller("socialmediaCtrl", function ($scope,$rootScope, socialmediaFactory) {
    
    $rootScope.HeadersName="Social Media Handles"
    
    $scope.NUMBER_OF_ITEMS = 10;
    $scope.PAGE_NUMBER = 1;
    $scope.relArticles = false
    $scope.previous = true
    $scope.next = true
    $scope.loader = true
    $scope.main = false
    $scope.totalHandleShow = false
    $scope.loader2 = true

    $scope.storeSocialHandles = function () {
        var handle = $scope.handle
        var website = $scope.website

        var promise = socialmediaFactory.storeSocialHandles(handle, website);
        promise.then(pass, fail);

        function pass(data) {
            window.alert(data.data.data)
            console.log(data.data.data)
        }

        function fail(er) {
            $scope.error = er;
            console.log(er);
        }
    }

    $scope.getAllSocialHandles = function () {
        var promise = socialmediaFactory.getAllSocialHandles($scope.PAGE_NUMBER, $scope.NUMBER_OF_ITEMS);
        promise.then(success, error);

        function success(data) {
            $scope.AllTwitterData = data.data.data;
            console.log(data.data.data)
            $scope.TotalHandles = data.data.data.length
            $scope.next = false
            $scope.main = true
            $scope.loader = false
            $scope.totalHandleShow =true;
            $scope.loader2=false;
        }

        function error(er) {
            $scope.ArticleError = er;
            console.log(er);
        }

    }

    $scope.NextPage = function () {

            $scope.PAGE_NUMBER += 1
            $scope.previous = false;
            var articlesend = $scope.NUMBER_OF_ITEMS * ($scope.PAGE_NUMBER - 1) + 10
            if (articlesend <= $scope.TotalArticlesLength) {
                $scope.getArticlesPromise();
            } else {
                $scope.next = true
            }
        },

        $scope.PreviousPage = function () {

            $scope.PAGE_NUMBER -= 1
            var articlesStart = $scope.NUMBER_OF_ITEMS * ($scope.PAGE_NUMBER - 1) + 1
            console.log(articlesStart)
            if (articlesStart <= 10) {
                $scope.previous = true
                $scope.getArticlesPromise();
            } else {
                $scope.getArticlesPromise();
                $scope.next = false;
            }

        }

    $scope.getAllSocialHandles()





})
