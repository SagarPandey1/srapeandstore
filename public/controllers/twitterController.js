app.controller("twitterCtrl", function ($scope,$rootScope, twitterfactory) {

    $rootScope.HeadersName="Tweets of the Day"
    
    $scope.NUMBER_OF_ITEMS = 10;
    $scope.PAGE_NUMBER = 1;
    $scope.relArticles = false
    $scope.previous = true
    $scope.next = true
    $scope.loader = true
    $scope.main = false
    $scope.totaltweetsShow = false
    $scope.loader2 = true

    $scope.getTweetsPromise = function () {

            var promise = twitterfactory.getTwitterData($scope.PAGE_NUMBER, $scope.NUMBER_OF_ITEMS);
            promise.then(success, error);

            function success(data) {
                $scope.tweet = data.data;
                console.log(data.data.data)
                $scope.next = false
                $scope.main = true
                $scope.loader = false
            }

            function error(er) {
                $scope.ArticleError = er;
                console.log(er);
            }


        },


        $scope.getTotalNoOfTweets = function () {

            var promise = twitterfactory.getNoOfTweets();
            promise.then(success, error);


            function success(data) {
                $scope.TotalTweets = data.data.data.length
                console.log(data.data.data)
                $scope.totaltweetsShow = true;
                $scope.loader2 = false;
            }

            function error(er) {
                $scope.ArticleError = er;
                console.log(er);
            }

        }
     $scope.showFullDetails = function (Object, index) {
            var id = Object.target.value;
            $scope.jsonDataView = !$scope.jsonDataView
            if ($scope.jsonDataView) {
                var promise = twitterfactory.showFullDetails(id);
                promise.then(pass, fail);

                function pass(data) {
                    $scope.myJSON = data.data.data
                    $scope.jsonData = index
                       
                }

                function fail(er) {
                    $scope.error = er;
                    console.log(er);
                }
            }
        },

    $scope.NextPage = function () {

            $scope.PAGE_NUMBER += 1
            $scope.previous = false;
            var articlesend = $scope.NUMBER_OF_ITEMS * ($scope.PAGE_NUMBER - 1) + 10
            if (articlesend < $scope.TotalTweets) {
                $scope.getTweetsPromise();
            } else {
                $scope.next = true
            }
        },

        $scope.PreviousPage = function () {

            $scope.PAGE_NUMBER -= 1
            var articlesStart = $scope.NUMBER_OF_ITEMS * ($scope.PAGE_NUMBER - 1) + 1
            console.log(articlesStart)
            if (articlesStart <= 10) {
                $scope.previous = true
                $scope.getTweetsPromise();
            } else {
                $scope.getTweetsPromise();
                $scope.next = false;
            }



        }


    $scope.getTotalNoOfTweets()
    $scope.getTweetsPromise()


})
