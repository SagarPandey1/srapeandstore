app.controller("kanoonCtrl", function ($scope,$rootScope, kanoonFactory) {
    $rootScope.HeadersName="Indian Kanoon Cases"
    
    $scope.NUMBER_OF_ITEMS = 10;
    $scope.PAGE_NUMBER = 1;
    $scope.relArticles = false
    $scope.previous = true
    $scope.next = true
    $scope.loader = true
    $scope.main = false
    $scope.articleslengthShow = false
    $scope.loader2 = true
    $scope.relLoader = -1;

    $scope.getArticlesPromise = function () {
            $scope.loader = true
            $scope.main = false
            var promise = kanoonFactory.getKanoonData($scope.PAGE_NUMBER, $scope.NUMBER_OF_ITEMS);
            promise.then(pass, fail);

            function pass(data) {

                $scope.article = data.data
                console.log(data.data.data)
                $scope.next = false
                $scope.next = false
                $scope.main = true
                $scope.loader = false

            }

            function fail(er) {
                $scope.error = er;
                console.log(er);
            }

        },

        $scope.tfidfSorting = function (termsArray, JsonData) {
            result = JsonData
            termsList = JSON.parse(termsArray)

            New_Array_List = []
            for (var m = 0; m < result.length; m++) {
                matchedElem = 0
                matchedArrayList = []
                for (var i = 0; i < result[m].WebInfo.urlsInfo.tfidf.length; i++) {

                    for (var j = 0; j < termsList.length; j++) {

                        if (result[m].WebInfo.urlsInfo.tfidf[i].TermsName == termsList[j].TermsName) {
                            matchedElem++;

                            matchedArrayList.push(result[m].WebInfo.urlsInfo.tfidf[i])
                        }

                    }
                }
                New_Array_List.push({
                    "WebSite": result[m].WebSite,
                    "Title": result[m].WebInfo.urlsInfo.Title,
                    "Url": result[m].WebInfo.urlsInfo.urlName,
                    "MatchedItem": matchedElem,
                    "MatchedTerms": matchedArrayList
                })
            }
            return New_Array_List

        }


    $scope.getRelatedArticles = function (event, index) {
        $scope.relArticles = !$scope.relArticles
        $scope.jsonDataView = false
        $scope.relLoader = index
        var termList = event.target.value;
        if ($scope.relArticles == true) {
            var promise = kanoonFactory.getRelatedIndianCases(termList);
            promise.then(pass, fail);

            function pass(data) {
                $scope.Relatedarticle = $scope.tfidfSorting(termList, data.data.data)
                $scope.relatedCases = $scope.Relatedarticle.length
                $scope.showTotalRelatedData = true;
                $scope.relLoader = -1
                $scope.showArticles = index
                console.log($scope.Relatedarticle)

            }

            function fail(er) {
                $scope.error = er;
                console.log(er);
            }
        }
    }


    $scope.getTotalNoOfArticles = function () {

        var promise = kanoonFactory.getTotalCases();
        promise.then(pass, fail);

        function pass(data) {
            $scope.TotalArticlesLength = data.data.data.data
            $scope.TotalPages = Math.trunc($scope.TotalArticlesLength/10+1)
            $scope.articleslengthShow = true;
            $scope.loader2 = false;

        }

        function fail(er) {
            $scope.error = er;
            console.log(er);
        }

    }

    $scope.showFullDetails = function (Object, index) {
            var title = Object.target.value;
           $scope.jsonDataView = !$scope.jsonDataView
            $scope.relArticles = false
            $scope.relLoader = index
            if ($scope.jsonDataView==true) {
                var promise = kanoonFactory.dumpKanoonDetails(title);
                promise.then(pass, fail);

                function pass(data) {
                    console.log(data.data.data)    
                    $scope.myJSON = data.data.data
                     $scope.relLoader = -1
                    $scope.jsonData = index


                }

                function fail(er) {
                    $scope.error = er;
                    console.log(er);
                }
            }
        },

        $scope.NextPage = function () {

            $scope.PAGE_NUMBER += 1
            $scope.previous = false;
            var articlesend = $scope.NUMBER_OF_ITEMS * ($scope.PAGE_NUMBER - 1) + 10
            if (articlesend <= $scope.TotalArticlesLength) {
                $scope.getArticlesPromise();
            } else {
                $scope.next = true
            }
        },

        $scope.PreviousPage = function () {

            $scope.PAGE_NUMBER -= 1
            var articlesStart = $scope.NUMBER_OF_ITEMS * ($scope.PAGE_NUMBER - 1) + 1
            console.log(articlesStart)
            if (articlesStart <= 10) {
                $scope.previous = true
                $scope.getArticlesPromise();
            } else {
                $scope.getArticlesPromise();
                $scope.next = false;
            }



        },
        $scope.getPage = function () {

            if (parseInt($scope.pageNo) > Math.trunc($scope.TotalArticlesLength / 10 + 1)) {
                window.alert("Please enter page number less then or equal to " + (parseInt($scope.TotalArticlesLength / 10) + 1));
            } else if (parseInt($scope.pageNo) == 0) {
                window.alert("Please enter page number greater then " + 0)
            } else {
                $scope.PAGE_NUMBER = parseInt($scope.pageNo);
                $scope.loader = true
                $scope.main = false
                var promise = kanoonFactory.getKanoonData($scope.PAGE_NUMBER, $scope.NUMBER_OF_ITEMS);
                promise.then(success, error);

                function success(data) {
                    $scope.article = data.data;
                    console.log(data.data.data)
                    $scope.next = false
                    $scope.main = true
                    $scope.loader = false
                    if ($scope.PAGE_NUMBER == 1) {
                        $scope.previous = true;
                    } else {
                        $scope.previous = false;
                    }
                    if ($scope.pageNo == Math.trunc($scope.TotalArticlesLength / 10 + 1)) {
                        $scope.next = true
                    }
                }

                function error(er) {
                    $scope.ArticleError = er;
                    console.log(er);
                }

            }
        }


    $scope.getTotalNoOfArticles()
    $scope.getArticlesPromise()









})
