app.factory("indexfactory", function ($http, $q, $window) {



    var Object = {

        getTotalDataInfo: function () {
            var deferred = $q.defer();
            $http({
                    url: indexUrl.TotalDataJSON,
                    method: 'get',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (data) {

                    deferred.resolve({
                        data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        err
                    });
                });
            return deferred.promise;
        },
        getTotalUrls: function () {
            var deferred = $q.defer();
            $http({
                    url: indexUrl.TotalUrlsJSON,
                    method: 'get',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (data) {

                    deferred.resolve({
                        data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        err
                    });
                });
            return deferred.promise;
        },
        getTotalArticles: function () {
            var deferred = $q.defer();
            $http({
                    url: indexUrl.TotalArticlesJSON,
                    method: 'get',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (data) {

                    deferred.resolve({
                        data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        err
                    });
                });
            return deferred.promise;
        },
        getTotalNouns: function () {
            var deferred = $q.defer();
            $http({
                    url: indexUrl.TotalNounsJSON,
                    method: 'get',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (data) {

                    deferred.resolve({
                        data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        err
                    });
                });
            return deferred.promise;
        },
        getTotalSentiments: function () {
            var deferred = $q.defer();
            $http({
                    url: indexUrl.TotalSentimentsJSON,
                    method: 'get',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (data) {

                    deferred.resolve({
                        data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        err
                    });
                });
            return deferred.promise;
        },
        getTotalHandles: function () {
            var deferred = $q.defer();
            $http({
                    url: indexUrl.TotalHandlesJSON,
                    method: 'get',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (data) {

                    deferred.resolve({
                        data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        err
                    });
                });
            return deferred.promise;
        },
        
        getTotalTweets: function () {
            var deferred = $q.defer();
            $http({
                    url: indexUrl.TotalTweetsJSON,
                    method: 'get',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (data) {

                    deferred.resolve({
                        data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        err
                    });
                });
            return deferred.promise;
        },



    }

    return Object






})
