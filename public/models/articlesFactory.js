app.factory("articlesFactory", function ($http, $q, $window) {

   
    var Object = {

        getArticles: function (pageNo, NoOfItems) {
            var obj = {
                "pageNO": pageNo,
                "NoOfItems": NoOfItems
            }
            var deferred = $q.defer();
            $http({
                    url: articlesUrl.ArticlesJSON,
                    method: 'get',
                    params: obj,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (data) {
                    deferred.resolve({
                        data: data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        data: err
                    });
                });
            return deferred.promise;
        },
        getRelatedArticles: function (values) {
            var obj = {
                "termName": values,
            };
            var newobj = JSON.stringify(obj);
            var deferred = $q.defer();
            $http({
                    url: articlesUrl.RelatedArticlesJSON,
                    method: 'get',
                    params: obj,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (data) {

                    deferred.resolve({
                        data: data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        data: err
                    });
                });
            return deferred.promise;
        },
        getNoOfResult: function () {
            var deferred = $q.defer();
            $http({
                    url: articlesUrl.TotalResultJSON,
                    method: 'get',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (data) {

                    deferred.resolve({
                        data: data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        data: err
                    });
                });
            return deferred.promise;
        },
        showFullDetails: function (object) {

            var obj = {
                "Title": object
            };
            var deferred = $q.defer();
            $http({
                    url: articlesUrl.DetailsJSON,
                    method: 'get',
                    params: obj,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (data) {
                    deferred.resolve({
                        data: data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        data: err
                    });
                });
            return deferred.promise;
        },
        getSortedArticlesByDate: function (pageNo, NoOfItems) {
            var obj = {
                "pageNO": pageNo,
                "NoOfItems": NoOfItems
            }
            var deferred = $q.defer();
            $http({
                    url: sortArticlesUrl.sortByDate,
                    method: 'get',
                    params: obj,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (data) {
                    deferred.resolve({
                        data: data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        data: err
                    });
                });
            return deferred.promise;
        },
         getSortedArticlesByPopularity: function (pageNo, NoOfItems) {
            var obj = {
                "pageNO": pageNo,
                "NoOfItems": NoOfItems
            }
            var deferred = $q.defer();
            $http({
                    url: sortArticlesUrl.sortByPopularity,
                    method: 'get',
                    params: obj,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (data) {
                    deferred.resolve({
                        data: data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        data: err
                    });
                });
            return deferred.promise;
        },
        getArticlesByDate: function (pageNo, NoOfItems,date) {
            var obj = {
               "pageNO": pageNo,
                "NoOfItems": NoOfItems,
                "date" : date
                
            }
            var deferred = $q.defer();
            $http({
                    url: filtersUrl.filterByDate,
                    method: 'get',
                    params: obj,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (data) {
                    deferred.resolve({
                        data: data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        data: err
                    });
                });
            return deferred.promise;
        },
        getNoOfResultFilterByDate :function(Date){
            console.log(Date)
            var obj ={"date":Date}
            var deferred = $q.defer();
            $http({
                    url: filtersUrl.TotalArticlesByDateJSON,
                    method: 'get',
                    params :obj,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (data) {

                    deferred.resolve({
                        data: data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        data: err
                    });
                });
            return deferred.promise;
        }

    }

    return Object


})
