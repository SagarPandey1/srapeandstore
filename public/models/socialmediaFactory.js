app.factory("socialmediaFactory", function ($http, $q, $window) {

    class Socialhandle {
        constructor(handle, website) {
            this.handle = handle
            this.website = website
        }
    }

    var Object = {

        getAllSocialHandles: function (pageNo, NoOfItems) {

            var obj = {
                "pageNO": pageNo,
                "NoOfItems": NoOfItems
            }
            var deferred = $q.defer();
            $http({
                    url: socialHandlesUrl.AllSocialHandlesJSON,
                    method: 'get',
                    params: obj,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (data) {

                    deferred.resolve({
                        data: data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        data: err
                    });
                });
            return deferred.promise;
        },
        storeSocialHandles: function (handle, website) {
            var obj = new Socialhandle(handle, website)

            var deferred = $q.defer();
            $http({
                    url: socialHandlesUrl.StoreSocialHandle,
                    method: 'get',
                    params: obj,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (data) {

                    deferred.resolve({
                        data: data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        data: err
                    });
                });
            return deferred.promise;
        },


    }

    return Object

})
