app.factory("twitterfactory", function ($http, $q, $window) {


    var Objects = {

        getTwitterData: function (pageNo, NoOfItems) {
            var obj = {
                "pageNO": pageNo,
                "NoOfItems": NoOfItems
            }
            var deferred = $q.defer();
            $http({
                    url: tweetDataJSON.AllTweetDataJSON,
                    method: 'get',
                    params: obj,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (data) {

                    deferred.resolve({
                        data: data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        data: err
                    });
                });
            return deferred.promise;

        },
        getNoOfTweets: function () {
            var deferred = $q.defer();
            $http({
                    url: tweetDataJSON.NoofTweetsJSON,
                    method: 'get',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (data) {

                    deferred.resolve({
                        data: data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        data: err
                    });
                });
            return deferred.promise

        },
        showFullDetails: function (object) {

            var obj = {
                "ID": object
            };
            console.log(obj)
            var deferred = $q.defer();
            $http({
                    url: tweetDataJSON.DetailsJSON,
                    method: 'get',
                    params: obj,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (data) {

                    deferred.resolve({
                        data: data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        data: err
                    });
                });
            return deferred.promise;
        },




    }


    return Objects


})
