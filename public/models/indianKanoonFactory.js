app.factory("kanoonFactory", function ($http, $q, $window) {
    
    
var Object={
    
    getKanoonData:function(pageNo,NoOfItems){
               var obj={"pageNO":pageNo,
                        "NoOfItems":NoOfItems}
             var deferred = $q.defer();
            $http({
                    url: indianKanoonUrl.KanoonDataJSON,
                    method: 'get',
                    params:obj,
                    headers: {
                         'Content-Type':'application/json'
                    }
                }).then(function (data) {
                    deferred.resolve({
                        data: data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        data: err
                    });
                });
            return deferred.promise;  
        },
  
      dumpKanoonDetails:function(object){
           var obj ={"Title":object};
             var deferred = $q.defer();
            $http({
                    url: indianKanoonUrl.KanoonDetailsJSON,
                    method: 'get',
                    params: obj,
                    headers: {
                         'Content-Type':'application/json'
                    }
                }).then(function (data) {

                    deferred.resolve({
                        data: data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        data: err
                    });
                });
            return deferred.promise; 
      },
    
      getRelatedIndianCases:function(values){
          
          var obj ={"nounName":values};
            var newobj=JSON.stringify(obj);
             var deferred = $q.defer();
            $http({
                    url: indianKanoonUrl.RelatedCasesJSON,
                    method: 'get',
                    params: obj,
                    headers: {
                         'Content-Type':'application/json'
                    }
                }).then(function (data) {

                    deferred.resolve({
                        data: data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        data: err
                    });
                });
            return deferred.promise;  
          
      },
      getTotalCases:function(){
           var deferred = $q.defer();
            $http({
                    url: indianKanoonUrl.TotalCasesResultJSON,
                    method: 'get',
                    headers: {
                         'Content-Type':'application/json'
                    }
                }).then(function (data) {

                    deferred.resolve({
                        data: data
                    });
                })
                .catch(function (err) {

                    deferred.reject({
                        data: err
                    });
                });
            return deferred.promise; 
          
          
      }
    
    
    
}    
    
    
 return Object   

    
    
    
})

    