
var express = require('express');
var router = express.Router();



router.get("/getUrls",function(req,res){

    var Opeartions=require("../../db/operations.js")
    Opeartions.getUrls(res);
})

router.get("/getArticles",function(req,res){
   var obj=req.query;
   var Opeartions=require("../../db/operations.js")
   Opeartions.getContents(obj,res)
   
})

router.get("/getNouns",function(req,res){
   var obj=req.query;
   var Opeartions=require("../../db/operations.js")
   Opeartions.getAllNouns(obj,res);
})

router.get("/getRelatedArticles",function(req,res){
   var obj=req.query;
   var Opeartions=require("../../db/operations.js")
   Opeartions.getRelatedArticles(obj,res);
})

router.get("/gettotalRelatedArticles",function(req,res){
    var obj=req.query;
    var Opeartions=require("../../db/operations.js")
    Opeartions.gettotalRelatedArticles(obj,res);
 })


router.get("/getFullDetails",function(req,res){
   var obj=req.query;
   var Opeartions=require("../../db/operations.js")
   Opeartions.getFullDetails(obj,res);
})
router.get("/getSocialHandle",function(req,res){ 
   var obj=req.query;
   let handle=req.query.handle;
   let website=req.query.website;

   const objectmaker=require("../../helper/SocialHelper.js")
   var object=new objectmaker(handle,website)
   var Opeartions=require("../../db/operations.js")
   Opeartions.getSocialHandle(object,res);
})

router.get("/getNoOfResults",function(req,res){
   var Opeartions=require("../../db/operations.js")
   Opeartions.getNoOfResult(res);
})
router.get("/getPrevArticles",function(req,res){
   var obj=req.query;
   var Opeartions=require("../../db/operations.js")
   Opeartions.getPrevArticles(obj,res)

})
router.get("/getTwitterData",function(req,res){
   var obj=req.query;
   var Opeartions=require("../../db/TwitterOperations")
   Opeartions.getTwitterData(obj,res)
})

router.get("/getPrevTwitterData",function(req,res){
   var obj=req.query;
   var Opeartions=require("../../db/TwitterOperations")
   Opeartions.getPrevTwitterData(obj,res)
})

router.get("/getNoOfTweets",function(req,res){
   var Opeartions=require("../../db/TwitterOperations")
   Opeartions.getNoOfTweets(res);
})

router.get("/getFullTweetDetails",function(req,res){
    var obj=req.query;
    var Opeartions=require("../../db/TwitterOperations")
    Opeartions.getFullTweetDetails(obj,res);
 })

router.get("/getAllSocialHandles",function(req,res){
   var obj=req.query;
   var Opeartions=require("../../db/TwitterOperations")
   Opeartions.getAllSocialHandles(obj,res)
})

router.get("/getArticlesTfIdf",function(req,res){
    var obj=req.query;
    var Opeartions=require("../../db/operations.js")
    Opeartions.getArticlesTfIdf(obj,res);
})

router.get("/indianKanoon",function(req,res){
    var obj=req.query;
    var Opeartions=require("../../db/indianKanoonOp")
    Opeartions.getAllIndianCases(obj,res);
})
router.get("/indianKanoonNouns",function(req,res){
    var obj=req.query;
    var Opeartions=require("../../db/indianKanoonOp")
    Opeartions.getAllNouns(obj,res);
 })

 router.get("/indianKanoonDump",function(req,res){
    var obj=req.query;
    var Opeartions=require("../../db/indianKanoonOp")
    Opeartions.getfullDetailsOfCases(obj,res);
 }) 
 
 router.get("/RelatedCases",function(req,res){
    var obj=req.query;
    var Opeartions=require("../../db/indianKanoonOp")
    Opeartions.getRelatedCases(obj,res);
 })

 router.get("/totalcases",function(req,res){
    var Opeartions=require("../../db/indianKanoonOp")
    Opeartions.getTotalcases(res);
 })

 router.get("/articlesSortByDate",function(req,res){
    var obj=req.query;
    var Opeartions=require("../../db/operations.js")
    Opeartions.getContentsSortByDate(obj,res)
    
 })

 router.get("/articlesSortByPopularity",function(req,res){
    var obj=req.query;
    var Opeartions=require("../../db/operations.js")
    Opeartions.getContentsSortByPopularity(obj,res)
    
 })

module.exports=router