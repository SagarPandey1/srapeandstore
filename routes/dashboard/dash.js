//////////////// This routes is for Dashboard Only  //////
 
var express = require('express');
var router = express.Router();

const dashOp=require("../../db/DashOp")


router.get('/totalUrls',function(request,response)         
{   
      dashOp.getTotalUrls(response)
      
})

router.get('/totalArticles',function(request,response)         
{   
         dashOp.getTotalArticles(response)
      
})

router.get('/totalNouns',function(request,response)         
{   
         dashOp.getTotalNouns(response)
      
})

router.get('/totalSentiments',function(request,response)         
{  
      dashOp.getTotalSentiments(response)
      
})


router.get('/totalHandles',function(request,response)         
{   
      dashOp.getTotalHandles(response)
      
})

router.get('/totalTweets',function(request,response)         
{   
      dashOp.getTotalTweets(response)
      
})



module.exports=router;