const urlschema = require("./urlSchema.js");
const contentSchema = require("./contentSchema.js")
const socialHandle = require("./socialMediaSchema.js")
const articlesSchema = require("../db/Schema/articlesSchema")



var operations = {

        getUrls(response) {
                urlschema.find({}, function (error, docs) {
                        if (error) {
                                response.send({
                                        mesasge: 'Some Problem Occur in Server Please try after Sometime....!'
                                });
                        } else {
                                if (docs.length == 0) {
                                        response.send("Nothing is Available....");
                                } else {
                                        response.send(JSON.stringify(docs));
                                }

                        }
                })
        },


        getContents(obj, response) {        //Old Collection API
                let pageno = JSON.parse(obj.pageNO)
                let noofitems = JSON.parse(obj.NoOfItems)
                let skipvalue = noofitems * (pageno - 1)
                let limitvalue = noofitems
                articlesSchema.find({
                                "WebInfo.urlsInfo.Content": {
                                        $ne: ""
                                }
                        }).skip(skipvalue).limit(limitvalue).sort({
                                "WebInfo.urlsInfo.fbStats.TotalLikes": -1
                        })
                        .exec(function (error, docs) {
                                if (error) {
                                        response.send({
                                                message: 'Some Problem Occur in Server Please try after Sometime....!'
                                        });
                                } else {
                                        if (docs.length == 0) {
                                                response.send("Nothing Content is Available....")
                                        } else {
                                                response.send(docs)
                                        }

                                }
                        })

        },
        getAllNouns(obj, response) {
                let name = obj.nounName;

                articlesSchema.find({
                        "WebInfo.urlsInfo.Nouns": name
                }, function (error, doc) {
                        if (error) {
                                response.send('Some Problem Occur in Server Please try after Sometime....!');
                        } else {

                                if (doc.length == 0) {

                                        response.send("Nothing Content is Available....")
                                } else {
                                        response.send(doc)
                                }

                        }


                })


        },
        getRelatedArticles(obj, response) {
                let name = obj.termName;
                let object = JSON.parse(name);
                articlesSchema.find({
                                "WebInfo.urlsInfo.tfidf": {
                                        '$in': object
                                },
                                "WebInfo.urlsInfo.Content": {
                                        $ne: ""
                                }
                        })
                        .exec(function (error, doc) {
                                if (error) {

                                        response.send('Some Problem Occur in Server Please try after Sometime....!');
                                } else {

                                        if (doc.length == 0) {

                                                response.send("Nothing Content is Available....")
                                        } else {
                                                let sortByTfIDf = require("../helper/Sort")
                                                relArticles = sortByTfIDf.sortByMaxTfIdf(object, doc)
                                                response.send(relArticles)

                                        }
                                }

                        })


        },
        getFullDetails(obj, response) {
                let title = obj.Title;
                // let object=JSON.parse(name);
               
                articlesSchema.find({
                        "WebInfo.urlsInfo.Title": title
                }, function (error, doc) {
                        if (error) {

                                response.send('Some Problem Occur in Server Please try after Sometime....!');
                        } else {

                                if (doc.length == 0) {

                                        response.send("Nothing Content is Available....")
                                } else {
                                       
                                        response.send(doc[0])

                                }


                        }

                })
        },
        getSocialHandle(obj, response) {
                socialHandle.create(obj, function (error) {
                        if (error) {
                                response.send("Can't Add this Information...!");
                        } else {
                                response.send("Your Information is Added Successfully...!");
                        }
                });
        },
        getNoOfResult(response) {
                articlesSchema.find({
                        "WebInfo.urlsInfo.Content": {
                                $exists: true
                        }
                }).count().exec(function (error, docs) {
                        if (error) {
                                response.send({
                                        message: 'Some Problem Occur in Server Please try after Sometime....!'
                                });
                        } else {
                                if (docs == 0) {
                                        response.send("Nothing Content is Available....")
                                } else {
                                        //console.log(docs.length)
                                        response.send({
                                                "length": docs
                                        })
                                }

                        }
                })

        },

        /*
        getPrevArticles(obj, response) {
                let pageno = JSON.parse(obj.pageNO)
                let noofitems = JSON.parse(obj.NoOfItems)
                let skipvalue = noofitems * (pageno - 1)
                let limitvalue = noofitems
                contentSchema.find({
                        "WebInfo.urlsInfo.Content": {
                                $exists: true
                        }
                }).skip(skipvalue).limit(limitvalue).exec(function (error, docs) {
                        if (error) {
                                response.send({
                                        message: 'Some Problem Occur in Server Please try after Sometime....!'
                                });
                        } else {
                                if (docs.length == 0) {
                                        response.send("Nothing Content is Available....")
                                } else {
                                        response.send(docs)
                                }

                        }
                })


        },       */
        getArticlesTfIdf(obj, response) {
                let pageno = JSON.parse(obj.pageNO)
                let noofitems = JSON.parse(obj.NoOfItems)
                let skipvalue = noofitems * (pageno - 1)
                let limitvalue = noofitems
                contentSchema.find({
                        "WebInfo.urlsInfo.Content": {
                                $exists: true
                        }
                }).skip(skipvalue).limit(limitvalue).exec(function (error, docs) {
                        if (error) {
                                response.send({
                                        message: 'Some Problem Occur in Server Please try after Sometime....!'
                                });
                        } else {
                                if (docs.length == 0) {
                                        response.send("Nothing Content is Available....")
                                } else {

                                        response.send(docs)
                                }

                        }
                })

        },

        /*----------------------- Filters Page API's Queries ------------------------------  */

        getArticlesByDate(obj, response) {
                let pageno = JSON.parse(obj.pageNO)
                let noofitems = JSON.parse(obj.NoOfItems)
                let date = obj.date
                let skipvalue = noofitems * (pageno - 1)
                let limitvalue = noofitems

                articlesSchema.find({
                                "WebInfo.urlsInfo.Date": date
                        })
                        .skip(skipvalue).limit(limitvalue).exec(function (error, docs) {
                                if (error) {
                                        response.send({
                                                message: 'Some Problem Occur in Server Please try after Sometime....!'
                                        });
                                } else {
                                        if (docs.length == 0) {

                                                response.send({
                                                        "message": "No Articles is found related to this date...",
                                                        "docLength": docs.length
                                                })
                                        } else {

                                                response.send(docs)
                                        }

                                }
                        })
        },
        foundNoOfArticlesByDate(obj, response) {
                let date = obj.date
                articlesSchema.find({
                                "WebInfo.urlsInfo.Date": date
                        }).count()
                        .exec(function (error, docs) {
                                if (error) {
                                        response.send({
                                                message: 'Some Problem Occur in Server Please try after Sometime....!'
                                        });
                                } else {
                                        if (docs == 0) {

                                                response.send({
                                                        "message": "No Articles is found related to this date...",
                                                        "docLength": docs
                                                })
                                        } else {

                                                response.send({
                                                        "totalLength": docs
                                                })
                                        }

                                }
                        })


        },


        /*----------------------- Articles Page SortBy API's Queries ------------------------------  */

        getContentsSortByDate(obj, response) {
                let pageno = JSON.parse(obj.pageNO)
                let noofitems = JSON.parse(obj.NoOfItems)
                let skipvalue = noofitems * (pageno - 1)
                let limitvalue = noofitems
                articlesSchema.find({
                                "WebInfo.urlsInfo.Content": {
                                        $ne: ""
                                }
                        }).sort({
                                "WebInfo.urlsInfo.Date": -1
                        })
                        .skip(skipvalue).limit(limitvalue).exec(function (error, docs) {
                                if (error) {
                                        response.send({
                                                message: 'Some Problem Occur in Server Please try after Sometime....!'
                                        });
                                } else {
                                        if (docs.length == 0) {
                                                console.log("hello")
                                                response.send("Nothing Content is Available....")
                                        } else {
                                                console.log(docs)
                                                response.send(docs)
                                        }

                                }
                        })

        },

        getContentsSortByDate(obj, response) {
                let pageno = JSON.parse(obj.pageNO)
                let noofitems = JSON.parse(obj.NoOfItems)
                let skipvalue = noofitems * (pageno - 1)
                let limitvalue = noofitems
                articlesSchema.find({
                                "WebInfo.urlsInfo.Content": {
                                        $ne: ""
                                }
                        }).sort({
                                "WebInfo.urlsInfo.Date": -1
                        })
                        .skip(skipvalue).limit(limitvalue).exec(function (error, docs) {
                                if (error) {
                                        response.send({
                                                message: 'Some Problem Occur in Server Please try after Sometime....!'
                                        });
                                } else {
                                        if (docs.length == 0) {
                                               
                                                response.send("Nothing Content is Available....")
                                        } else {
                                              
                                                response.send(docs)
                                        }

                                }
                        })
        },
        getContentsSortByPopularity(obj, response) {
                let pageno = JSON.parse(obj.pageNO)
                let noofitems = JSON.parse(obj.NoOfItems)
                let skipvalue = noofitems * (pageno - 1)
                let limitvalue = noofitems
                articlesSchema.find({
                                "WebInfo.urlsInfo.Content": {
                                        $ne: ""
                                }
                        }).sort({
                                "WebInfo.urlsInfo.fbStats.TotalLikes": -1
                        })
                        .skip(skipvalue).limit(limitvalue).exec(function (error, docs) {
                                if (error) {
                                        response.send({
                                                message: 'Some Problem Occur in Server Please try after Sometime....!'
                                        });
                                } else {
                                        if (docs.length == 0) {
                                                response.send("Nothing Content is Available....")
                                        } else {
                                                response.send(docs)
                                        }

                                }
                        })

        },

        



}

module.exports = operations