const mongoose = require("./connection");
var Schema = mongoose.Schema;

var contentSchema = new Schema({ "WebSite" : String,
"WebInfo" : {
    "urlsInfo" : {
        "Category" : String,
        "Author" : String,
        "Title" : String,
        "Content" : String,
        "Date" : String,
        "urlName" : String,
        "Nouns" : [],
        "fbID" : Number,
        "fbStats" :Object,
        "TopIdf" :[],
        "tfidf"   : [],
        

    }
}
                                    
                      });

var events = mongoose.model("articles",contentSchema);  


module.exports = events;

