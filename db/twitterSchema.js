const mongoose = require("./connection");
require('mongoose-long')(mongoose);
var Schema = mongoose.Schema;
var SchemaTypes = mongoose.Schema.Types;

var contentSchema = new Schema({ 
                                 "Tweets" : String,
                                 "Date_Created" : String,
                                  "url" : String,
                                  "handle" : String,
                                  "ID" : SchemaTypes.Long
                      });

var twitterSchema = mongoose.model("twitterdatas",contentSchema);  


module.exports = twitterSchema;