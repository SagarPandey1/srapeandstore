const mongoose = require("../connection.js");
var Schema = mongoose.Schema;

var contentSchema = new Schema({ "WebSite" : String,
"WebInfo" : {
    "urlsInfo" : {
        "Content" : String,
        "Date" : Date,
        "urlName" : String,
        "ImageSource" : String,
        "Tags" : String,
        "Category" : String,
        "Author" : String,
        "BaseUrl" : String,
        "TimeStamp" : Date,
        "CrawledAt" : Date,
        "Title" : String,
        "Nouns" : [],
        "fbID" : Number,
        "fbStats" :Object,
        "TopIdf" :[],
        "tfidf"   : [],  
        
    }
}
                                    
                      });

var events1 = mongoose.model("updatedarticles",contentSchema);  

module.exports = events1;