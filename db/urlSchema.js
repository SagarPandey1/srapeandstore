const mongoose = require("./connection");
var Schema = mongoose.Schema;

var urlSchema = new Schema({  
    "Website" :String,
    "SiteURL" : [],
    "SelectivesInfo" : {
        "Date" : {
            "ClassName" : String,
            "SelectiveName" : String,
            "getDate" : String
        },
        "Category" : {
            "ClassName" : String,
            "SelectiveName" : String
        },
        "Author" : {
            "ClassName" : String,
            "SelectiveName" : String
        },
        "ArticleBody" : {
            "SelectiveName" : String,
            "getClassNameForScience" : String,
            "getClassNameForBusiness" : String,
            "getClassNameForBackChannel" : String,
            "ClassName" : String,
            "getClassNameForCulture" : String
        },
        "Title" : {
            "ClasName" : String,
            "SelectiveName" : String
        }
    }
});

var events = mongoose.model("urls",urlSchema);    

module.exports = events;