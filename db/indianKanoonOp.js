const indianKanoon=require("../db/indianKanoonSchema")

var indianCasesOp={


         getAllIndianCases(obj,response){
            let pageno=JSON.parse(obj.pageNO)
            let noofitems=JSON.parse(obj.NoOfItems)
            let skipvalue=noofitems * (pageno - 1)
            let limitvalue=noofitems
           indianKanoon.find({ "WebInfo.urlsInfo.Content": { $exists: true} }).skip(skipvalue).limit(limitvalue)
                                             .exec(function(err,docs){
                                                if(err){
                                                    response.send({
                                                        message:'Some Problem Occur in Server Please try after Sometime....!'});
                                                }
                                                else{
                                                    if(docs.length==0){
                                                      response.send("Nothing Content is Available....")
                                                    }
                                                    else{   
                                                           response.send(docs)
                                                    }    
                                                                 
                                             }  })

             },
             indianKanoonOp(obj,response){

                let name=obj.nounName;
             
                indianKanoon.find({"WebInfo.urlsInfo.Nouns": name},function(error,doc){
                if(error){
                 response.send('Some Problem Occur in Server Please try after Sometime....!');
                    }
                    else{
                               
                        if(doc.length==0){
                                
                          response.send("Nothing Content is Available....")
                        }
                        else{   
                              response.send(doc)
                        }    
                                     
                 }


                })
             },
             getfullDetailsOfCases(obj,response){
                let title=obj.Title;
                indianKanoon.find({"WebInfo.urlsInfo.Title":title},{"WebInfo.urlsInfo.tfidf":0},function(error,doc){
                         if(error){
                                
                          response.send('Some Problem Occur in Server Please try after Sometime....!');
                             }
                             else{
                                        
                                 if(doc.length==0){
                                               
                                   response.send("Nothing Content is Available....")
                                 }
                                 else{   
                                        
                                        response.send(doc[0])
                                        
                                       }  
                                       
                                        
                                 }    
                                              
                         }) 

             },
             getRelatedCases(obj,response){
              let name=obj.nounName;
              let object=JSON.parse(name);
              indianKanoon.find({"WebInfo.urlsInfo.tfidf":{'$in':object}},function(error,doc){
                      if(error){
                             
                       response.send('Some Problem Occur in Server Please try after Sometime....!');
                          }
                          else{
                                     
                              if(doc.length==0){
                                            
                                response.send("Nothing Content is Available....")
                              }
                              else{   
                                    
                                     response.send(doc)
                                     
                                    }  
                                    
                                     
                              }    
                                           
                      })  
             },
             getTotalcases(response){
              indianKanoon.find({}).count().exec(function(error,doc){
                      if(error){
                             
                       response.send('Some Problem Occur in Server Please try after Sometime....!');
                          }
                          else{
                                     
                              if(doc==0){
                                            
                                response.send("Nothing Content is Available....")
                              }
                              else{   
                      
                                     response.send({
                                                     "data":doc,
                                                    "status":200})
                                        
                              }    
                                           
                      }}) 

             }


            }

module.exports=indianCasesOp            