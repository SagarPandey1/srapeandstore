const twitterschema=require("../db/twitterSchema.js")
const socialMediaSchema=require("../db/socialMediaSchema.js")
var mongoose = require('mongoose')
require('mongoose-long')(mongoose);
var Long = mongoose.Types.Long

var operations={
 
    getTwitterData(obj,response){
        let pageno=JSON.parse(obj.pageNO)
        let noofitems=JSON.parse(obj.NoOfItems)
        let skipvalue=noofitems * (pageno - 1)
        let limitvalue=noofitems
        twitterschema.find({}).skip(skipvalue).limit(limitvalue).exec(function(error,docs){
                if(error){
                    response.send({
                        message:'Some Problem Occur in Server Please try after Sometime....!'});
                }
                else{ 
                    
                    if(docs.length==0){
                      response.send("Nothing Content is Available....")
                    }
                    else{   
                           response.send(docs)
                    }    
                                 
             }  })  
        
        },
        getPrevTwitterData(obj,response){
            let pageno=JSON.parse(obj.pageNO)
            let noofitems=JSON.parse(obj.NoOfItems)
            let skipvalue=noofitems * (pageno - 1)
            let limitvalue=noofitems
            twitterschema.find({}).skip(skipvalue).limit(limitvalue).exec(function(error,docs){
                    if(error){
                        response.send({
                            message:'Some Problem Occur in Server Please try after Sometime....!'});
                    }
                    else{ 
                        
                        if(docs.length==0){
                          response.send("Nothing Content is Available....")
                        }
                        else{   
                               response.send(docs)
                        }    
                                     
                 }  })  

        },

        getNoOfTweets(response){
            twitterschema.find({}).count().exec(function(error,docs){
                if(error){
                    response.send({
                        message:'Some Problem Occur in Server Please try after Sometime....!'});
                }
                else{
                    if(docs==0){
                      response.send("Nothing Content is Available....")
                    }
                    else{   
                            //console.log(docs.length)
                           response.send({
                                   "length":docs})
                    }    
                                 
             }  })



        },
        getAllSocialHandles(obj,response){
            let pageno=JSON.parse(obj.pageNO)
            let noofitems=JSON.parse(obj.NoOfItems)
            let skipvalue=noofitems * (pageno - 1)
            let limitvalue=noofitems
            socialMediaSchema.find({}).skip(skipvalue).limit(limitvalue).exec(function(error,docs){
                    if(error){
                        response.send({
                            message:'Some Problem Occur in Server Please try after Sometime....!'});
                    }
                    else{ 
                        
                        if(docs.length==0){
                          response.send("Nothing Content is Available....")
                        }
                        else{   
                               response.send(docs)
                        }    
                                     
                 }  })  


        },

        getFullTweetDetails(obj,response){
            let id=obj.ID;
            console.log(id)
            twitterschema.find({"TweetsData.ID":Long.fromString(id)},function(error,doc){
                    if(error){
                           
                     response.send('Some Problem Occur in Server Please try after Sometime....!');
                        }
                        else{
                                   
                            if(doc.length==0){
                                          
                              response.send("Nothing Content is Available....")
                            }
                            else{   
                                  
                                   response.send(doc)
                                   
                                  }  
                                  
                                   
                            }    
                                         
                    }) 
    },
    
    
    
    
    
    
    
    }

module.exports=operations        