const twitterschema = require("./twitterSchema.js")
const socialMediaHandle = require("./socialMediaSchema.js")
const contentSchema = require("./contentSchema")
const urlSchema = require("./urlSchema")




var dashOperations = {

    getTotalUrls(response) {
        contentSchema.aggregate([{
                $match: {
                    "WebInfo.urlsInfo.Content": {
                        $exists: true
                    }
                }
            },
            {
                $group: {
                    _id: "$WebSite",
                    "count": {
                        $sum: 1
                    }
                }
            }
        ]).exec(
            function (error, docs) {
                if (error) {
                    response.send({
                        message: 'Some Problem Occur in Server Please try after Sometime....!'
                    });
                } else {
                    if (docs.length == 0) {
                        response.send("Nothing Content is Available....")
                    } else {
                        response.send(docs)
                    }

                }
            })
    },

    getTotalArticles(response) {

        contentSchema.aggregate([{
                $match: {
                    "WebInfo.urlsInfo.Content": {
                        $exists: true
                    }
                }
            },
            {
                $group: {
                    _id: "$WebSite",
                    "count": {
                        $sum: 1
                    }
                }
            }
        ]).exec(
            function (error, docs) {
                if (error) {
                    response.send({
                        message: 'Some Problem Occur in Server Please try after Sometime....!'
                    });
                } else {
                    if (docs.length == 0) {
                        response.send("Nothing Content is Available....")
                    } else {

                        response.send(docs)

                    }

                }
            })
    },

    getTotalNouns(response) {

        contentSchema.aggregate([{
                    $match: {
                        "WebInfo.urlsInfo.Content": {
                            $exists: true
                        }
                    }
                },
                {
                    $unwind: "$WebInfo.urlsInfo.Nouns"
                }, {
                    $group: {
                        _id: "$WebSite",
                        count: {
                            $sum: 1
                        }
                    }
                }
            ])
            .exec(
                function (error, docs) {
                    if (error) {
                        response.send({
                            message: 'Some Problem Occur in Server Please try after Sometime....!'
                        });
                    } else {
                        if (docs.length == 0) {
                            response.send("Nothing Content is Available....")
                        } else {
                            response.send(docs)
                        }

                    }
                })
    },

    getTotalSentiments(response) {
        contentSchema.aggregate([{
                $match: {
                    "WebInfo.urlsInfo.tfidf": {
                        $exists: true
                    }
                }
            },
            {
                $unwind: "$WebInfo.urlsInfo.tfidf"
            }, {
                $group: {
                    _id: "$WebSite",
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(
            function (error, docs) {
                if (error) {
                    response.send({
                        message: 'Some Problem Occur in Server Please try after Sometime....!'
                    });
                } else {
                    if (docs.length == 0) {
                        response.send("Nothing Content is Available....")
                    } else {

                        response.send(docs)
                    }

                }
            })
    },


    getTotalHandles(response) {

        socialMediaHandle.aggregate([{
                $match: {
                    "webSite": {
                        $exists: true
                    }
                }
            },
            {
                $group: {
                    _id: "$webSite",
                    "count": {
                        $sum: 1
                    }
                }
            }
        ]).exec(function (error, docs) {
            if (error) {
                totalHandles = 'Some Problem Occur in Server Please try after Sometime....!'
            } else {
                if (docs.length == 0) {
                    totalHandles = "Nothing Content is Available...."
                } else {
                    response.send(docs)
                }

            }

        })
    },
    getTotalTweets(response) {

        twitterschema.aggregate([{
                $match: {
                    "TweetsData.handle": {
                        $exists: true
                    }
                }
            },
            {
                $group: {
                    _id: "$TweetsData.handle",
                    "count": {
                        $sum: 1
                    }
                }
            }
        ]).exec(function (error, docs) {
            if (error) {
                totaltweets = 'Some Problem Occur in Server Please try after Sometime....!'
            } else {
                if (docs.length == 0) {
                    totaltweets = "Nothing Content is Available...."
                } else {

                    response.send(docs)

                }

            }
        })
    },

}

module.exports = dashOperations