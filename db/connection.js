const dbConfig = require("./config");       

const mongoose = require("mongoose");

const conn = mongoose.connect(dbConfig.dbURL)

/*
mongoose.connection.on('open', function () {
    mongoose.connection.db.listCollections().toArray(function (err, names) {
        console.log(err, names);
        
    });
});           */

console.log("Connection Created....");

module.exports = mongoose;