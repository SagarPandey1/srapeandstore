from bs4 import BeautifulSoup
from db import storeAbsoluteAndRelativeLinksDB
import urllib2
import re
import time
from db import getRelativeLinksFromDB
from db import finalStoringofAnchorsInDB
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import httplib

urlsArray = [#{"WebSiteName": "thenextweb", "url": 'https://thenextweb.com'},
             #{"WebSiteName": "firstpost", "url": 'http://www.firstpost.com'},
             #{"WebSiteName": "gizmodo", "url": 'https://gizmodo.com'},
             #{"WebSiteName": "mashable", "url": 'https://mashable.com'},
             #{"WebSiteName": "wired", "url": 'https://www.wired.com'},
             #{"WebSiteName": "yourstory", "url": 'https://yourstory.com'},
             #{"WebSiteName": "youngisthan", "url": 'https://www.youngisthan.in/'}
             #{"WebSiteName":"indiankanoon","url":'https://indiankanoon.org/browse/'}
             #{"WebSiteName": "youthkiawaaz", "url": 'https://www.youthkiawaaz.com/'},
             #{"WebSiteName": "mensxp", "url": 'https://www.mensxp.com/'}
             {"WebSiteName": "thelogicalindian", "url": 'https://thelogicalindian.com'}
             ]


# Header's Information....
user_agent = '5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36'
headers = {'User-Agent': user_agent}
data = None


Newlist = []


def request():
    urlslist = []
    for extralink in urlsArray:
        print("Fetching Anchors Tag from "+extralink['WebSiteName'])
        request = urllib2.Request(extralink['url'], data, headers)
        try:
            res = urllib2.urlopen(request)
        except (urllib2.HTTPError, urllib2.URLError) as err:
            continue

        links = []
        relativelinks = []
        if res.getcode() == 200:
            page = res.read()
            soup = BeautifulSoup(page, "html5lib")
            href = soup.findAll('a', href=True)
            for link in href:
                if(link['href'].startswith("#")):
                    continue
                elif(link['href'].startswith("/")):
                    relativelinks.append(link['href'])
                else:
                    links.append(link['href'])
        else:
            print("there is Problem while Fetching from" +
                  extralink['WebSiteName'])

        print("Fetching Anchors Tag from " +
              extralink['WebSiteName']+" "+"Completed..")
        links = (list(set(links)))
        #print(links)
        urlslist.append({
            "BaseUrl": extralink['url'],
            "Website": extralink['WebSiteName'],
            "SiteURL": links,
            "relativelinks": relativelinks
        })
    return urlslist


def getAbsoluteAndRelativeLinks():
    urlslist = request()
    #print(urlslist)
    storeAbsoluteAndRelativeLinksDB(data=urlslist)


def request2(links):
    for siteinfo in links:
        newLinks = []
        print("Fetching Relative urls from "+siteinfo['Website'])
        base_URL = siteinfo['BaseUrl']
        linksArray = []

        #--------Get Anchors from Relative urls of Website----
        for eachurl in siteinfo['RelativeLinks']:
            if(eachurl.startswith("/")):
                Absolute_URL = base_URL+eachurl
                request = urllib2.Request(Absolute_URL, data, headers)
                try:
                    res = urllib2.urlopen(request)
                    if res.getcode() == 200:
                        page = res.read()
                        soup = BeautifulSoup(page, "html5lib")
                        href = soup.findAll('a', href=True)
                        for link in href:
                            if(link['href'].startswith("/")):
                                Absolute_URL = base_URL+link['href']
                                linksArray.append(Absolute_URL)
                                print(Absolute_URL)
                            else:
                                if(re.findall('^(http|https)://', link['href'])):
                                    if(link['href'].startswith(base_URL)):

                                        linksArray.append(link['href'])
                                        print(link['href'])
                                    else:
                                        continue
                                else:
                                    continue
                    else:
                        print("there is Problem while Fetching from" +
                              siteinfo['Website'])
                except (urllib2.HTTPError, urllib2.URLError, httplib.BadStatusLine,httplib.IncompleteRead) as err:
                    continue
            obj = {
            "Website": siteinfo['Website'],
            "SiteURL": linksArray
               }
            newLinks.append(obj)
            finalStoringofAnchorsInDB(data=newLinks)
            linksArray=[]
            newLinks =[]        
        #print("Crawl is paused for  website "+" " +
                  #siteinfo['Website']+""+" and shall continue after 5sec ..")
        #time.sleep(5)

        #---------Get Anchors from Again Absolute Urls-------------
        for eachurl in siteinfo['SiteURL']:

            if(eachurl.startswith("/")):
                Absolute_URL = base_URL+eachurl
                request = urllib2.Request(Absolute_URL, data, headers)
                try:
                    res = urllib2.urlopen(request)
                    if res.getcode() == 200:
                        page = res.read()
                        soup = BeautifulSoup(page, "html5lib")
                        href = soup.findAll('a', href=True)
                        for link in href:
                            if(link['href'].startswith("/")):
                                Absolute_URL = base_URL+link['href']
                                linksArray.append(Absolute_URL)
                                print(Absolute_URL)
                            else:
                                if(re.findall('^(http|https)://', link['href'])):
                                    if(link['href'].startswith(base_URL)):

                                        linksArray.append(link['href'])
                                        print(link['href'])
                                    else:
                                        continue
                                else:
                                    continue
                    else:
                        print("there is Problem while Fetching from" +
                              siteinfo['Website'])
                except (urllib2.HTTPError, urllib2.URLError,httplib.IncompleteRead) as err:
                    continue
            #print("Crawl is paused for  website "+""+siteinfo['Website']+""+" and shall continue after 5sec ..")
            #time.sleep(5)
            linksArray = (list(set(linksArray)))
            print(linksArray)   

            obj = {
            "Website": siteinfo['Website'],
            "SiteURL": linksArray
               }
            newLinks.append(obj)
            finalStoringofAnchorsInDB(data=newLinks)
            linksArray=[]
            newLinks =[]

            if(eachurl.startswith("http") or eachurl.startswith("https")):
                if(not eachurl.startswith(base_URL)):
                    continue
                request = urllib2.Request(eachurl, data, headers)
                try:
                    res = urllib2.urlopen(request)
                    if res.getcode() == 200:
                        page = res.read()
                        soup = BeautifulSoup(page, "html5lib")
                        href = soup.findAll('a', href=True)
                        for link in href:
                            if(link['href'].startswith("/")):
                                Absolute_URL = base_URL+link['href']
                                m = linksArray.append(Absolute_URL)
                                # print(m)
                            else:
                                if(re.findall('^(http|https)://', link['href'])):
                                    if(link['href']):

                                        linksArray.append(link['href'])
                                        print(link['href'])
                                    else:
                                        continue
                                else:
                                    continue
                    else:
                        print("there is Problem while Fetching from" +
                              siteinfo['Website'])
                except (urllib2.HTTPError, urllib2.URLError, httplib.BadStatusLine,httplib.IncompleteRead) as err:
                    continue
            #print("Crawl is paused for  website "+" " +
                  #siteinfo['Website']+""+" and shall continue after 5sec ..")
            #time.sleep(5)
                obj = {
                "Website": siteinfo['Website'],
                "SiteURL": linksArray
                }
                newLinks.append(obj)
                finalStoringofAnchorsInDB(data=newLinks)
                linksArray=[]
                newLinks =[]

            else:
                Absolute_URL = base_URL+'/'+eachurl
                print(Absolute_URL)
                request = urllib2.Request(Absolute_URL, data, headers)
                try:
                    res = urllib2.urlopen(request)
                    if res.getcode() == 200:
                        page = res.read()
                        soup = BeautifulSoup(page, "html5lib")
                        href = soup.findAll('a', href=True)
                        for link in href:
                            if(link['href'].startswith("/")):
                                Absolute_URL1 = base_URL+link['href']
                                print(Absolute_URL1)
                                linksArray.append(Absolute_URL1)
                                
                            else:
                                if(re.findall('^(http|https)://', link['href'])):
                                    if(link['href'].startswith(base_URL)):

                                        linksArray.append(link['href'])
                                        print(link['href'])
                                    else:
                                        continue
                                else:
                                    continue
                    else:
                        print("there is Problem while Fetching from" +
                              siteinfo['Website'])
                except (urllib2.HTTPError, urllib2.URLError,httplib.IncompleteRead) as err:
                    continue
                

            obj = {
            "Website": siteinfo['Website'],
            "SiteURL": linksArray
               }
            newLinks.append(obj)
            finalStoringofAnchorsInDB(data=newLinks)
            linksArray=[]
            newLinks =[]

    return newLinks


def getAnchorsFromRelativeLinks():
    urllist = getRelativeLinksFromDB()
    #print(urllist)
    request2(links=urllist)



getAbsoluteAndRelativeLinks()
getAnchorsFromRelativeLinks()
