
# -*- coding: utf-8 -*-
import argparse
import re, cgi

from db import getJSONFromDBForSentimentTest
import nltk
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from nltk.tokenize import sent_tokenize
from db import sentimentsForTitle_DB
from db import sentimentsForScentences_DB
from db import sentimentsForArticles_DB

def textSpliter(Scentence):
    all_sent = sent_tokenize(Scentence)
    #print(all_sent)
    return all_sent
    
     

def filterArticlesBody(text):                       #2. Second this Run
    tag_re = re.compile(r'(<!--.*?-->|<[^>]*>)')
    no_tags = tag_re.sub('', text)
    ready_for_web = cgi.escape(no_tags)
    sentence = ready_for_web
    scentence =" ".join(sentence.split())
    SplitScentences=textSpliter(Scentence=scentence)
    #print(scentence)
    return scentence

def remove_Duplicate_Titles(titlesList):
    return list(set(titlesList))
    
       

def sentimentAnalysisForTitle(data):
    TitlesArray=remove_Duplicate_Titles(titlesList=data)
    titleswithSentiments=[]
    sid = SentimentIntensityAnalyzer()
    for title in TitlesArray:
        ss = sid.polarity_scores(title)
        titleswithSentiments.append({
                       "Title":title,
                       "Sentiment":ss
             })           
    #print(titleswithSentiments)
    return titleswithSentiments


def sentimentAnalysisForArticles(data):
    ArticleswithSentiments=[]
    for articles in data:
        Curr_Title=articles['Titles']
        sid = SentimentIntensityAnalyzer()
        for articleArray in articles['Scentences']:
            ss = sid.polarity_scores(articleArray)
            ArticleswithSentiments.append({
                       "Title":Curr_Title,
                       "Sentimentals":
                                  {"Scentence":articleArray,
                                   "Sentiment":ss
                                }         
             }) 
    #print(ArticleswithSentiments)
    return ArticleswithSentiments         
    
def sentimentAnalysisForWholeArticle(data):
    WholeArticleswithSentiments=[]
    for articles in data:
        Curr_Title=articles['Titles']
        sid = SentimentIntensityAnalyzer()
        ss = sid.polarity_scores(articles['Scentences'])
        WholeArticleswithSentiments.append({
                       "Title":Curr_Title,
                       "Sentimentals":
                                  {"Scentence":articles['Scentences'],
                                   "Sentiment":ss
                                }         
             })
    #print(ArticleswithSentiments)
    return WholeArticleswithSentiments      
       

def sendForSentimentAnalysis():              # 1. First this Run
    New_Array_List=[]
    New_Titles_List=[]
    New_Content_List=[]
    Filtered_Articles=[]
    AllData=getJSONFromDBForSentimentTest()
    for allData in AllData:
        New_Array_List.append({
              "Title":allData['WebInfo']['urlsInfo']['Title'],
              "Article":allData['WebInfo']['urlsInfo']['Content']
        })
    for data in New_Array_List:
            New_Titles_List.append(data['Title'])

    for data in New_Array_List:
       FilteredArticles=filterArticlesBody(text=data['Article'])
       Filtered_Articles.append({
                             "Titles":data['Title'],
                           "Scentences":FilteredArticles
       })
    for data in Filtered_Articles:
        SplitScentences = textSpliter(Scentence=data['Scentences'])   
        New_Content_List.append({
                          "Titles":data['Titles'],
                           "Scentences":SplitScentences
        })   

    WholeArticleSentiment = sentimentAnalysisForWholeArticle(data=Filtered_Articles)
    #print(WholeArticleSentiment)  
     
    
    SentimentsFromTitles    = sentimentAnalysisForTitle(data=New_Titles_List)
    #print(SentimentsFromTitles)
   
    SentimentsFromScentence = sentimentAnalysisForArticles(data=New_Content_List)
    #print(SentimentsFromScentence)
    #print(New_Content_List)
    
    sentimentsForTitle_DB(data=SentimentsFromTitles) 
    sentimentsForScentences_DB(data=SentimentsFromScentence)
    sentimentsForArticles_DB(data=WholeArticleSentiment)  


sendForSentimentAnalysis()                      #3. Third this Run


