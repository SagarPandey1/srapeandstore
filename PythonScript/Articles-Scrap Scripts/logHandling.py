import logging

def logSettings():
    logger = logging.getLogger('Logs')
    hdlr = logging.FileHandler('/home/sagar/Desktop/Project/PythonLogs/Logs.log')
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr) 
    logger.setLevel(logging.DEBUG)
    return logger