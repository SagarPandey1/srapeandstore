from bs4 import BeautifulSoup
from nltk.tag import pos_tag
from db import fetchingURLsList
from db1 import storingScrappedItemsOfArticlesinDB
from db1 import fetchingLastLevelsofUrlFromDB
import urllib
import time
 
    

DetailsArray=[]

def scrapArticlesContentsFromURLs():
    siteObject=fetchingLastLevelsofUrlFromDB()
    
    for obj in siteObject:
        for doc in obj['RelatedURL']['ThirdUrl']:
            for link in doc['links']:
                siteURL=link
                date=" "
                author=" "
                category=" "
                content=" "
                websiteName=obj['Website']
        
                urls =urllib.urlopen(link)
                pageDetails=urls.read()
                Sitesoup = BeautifulSoup(pageDetails, "html5lib")

                if(Sitesoup.find("div", attrs = {'class':"judgments"})!=None):
                    Title = Sitesoup.find("div", attrs = {'class':"judgments"}).find("div", attrs = {'class':"doc_title"})
                    title=Title.text.strip()


                Date = Sitesoup.find("div", attrs = {'class':"1"})
                Author =Sitesoup.find("div", attrs = {'class':"doc_author"})
                Category =Sitesoup.find("div", attrs = {'class':"1"})   

                if(Date!=None and Title!=None):
                    if(Date.text.strip()==""):
                        date=" "
                    else:
                        date=Date.text.strip() 

                if(Author!=None):
                    author=Author.text
                    

                if(Category!=None):
                    category=Category.text.strip()
                    #print(category)

                if(Sitesoup.find("div", attrs = {'class':"judgments"})!=None and Title!=None):
                    Content= Sitesoup.find("div", attrs = {'class':"judgments"})
                    content=Content.text.strip()
                    #print(content)  

                else:
                    continue 
                    

                storingScrappedItemsOfArticlesinDB(websitename=websiteName,url=siteURL,title=title,Date=date,
                            author=author,category=category,Content=content)    
                
    
                print(websiteName)
                print(date)
                print(category)
                print(siteURL)
                print(author)
                print(content)
                print(" ")
                
                
                     
                               
                        
                        
scrapArticlesContentsFromURLs()    
                

    