import datetime
now = datetime.datetime.now()
import json
import re, cgi
from bson import json_util
from logHandling import logSettings
logger=logSettings()
import time

def establishMongoConnection():
   from pymongo import MongoClient
   client = MongoClient() 
   client = MongoClient('mongodb://localhost:27017')
   db = client['pythonNLP']                               # DB name 'pythonNLP
   return db

def storingAnchorTags(data):
    db=establishMongoConnection()
    logger.debug("Mongo Connection Establish Successfully...")
    Anchor_Tag_List =data
    #print(Anchor_Tag_List)
    
    post=db.urls
    for anchorList in Anchor_Tag_List:
        post.insert_one({
             "SiteURL":anchorList['AllURLs'],
             "Website":anchorList['WebSiteName']
        })
    logger.debug("All Anchor's Tag are stored in Mongo Successfully...")
    

#storingAnchorTags()

def UpdatingAnchorTags(data):
    db=establishMongoConnection()
    logger.debug("Mongo Connection Establish Successfully...")
    Anchor_Tag_List =data
    #print(Anchor_Tag_List)
    
    post=db.urls
    for anchorList in Anchor_Tag_List:
        post.update({"Website":anchorList['WebSiteName']},
                            {"$addToSet" :
                                    {"SiteURL":{"$each" :anchorList['AllURLs']} }  }
                                       )
    logger.debug("All Anchor's Tag are updated in Mongo Successfully...")
    print("New Anchor's Added Success")

#UpdatingAnchorTags()

def storeAbsoluteAndRelativeLinksDB(data):
    db=establishMongoConnection()
    logger.debug("Mongo Connection Establish Successfully...")
    Anchor_Tag_List =data
    post=db.urls
    for anchorList in Anchor_Tag_List:
        post.update({"Website":anchorList['Website']},
                            {"$addToSet" :
                                    {"RelativeLinks":{"$each" :anchorList['relativelinks']} }  }
                                       )
        post.update({"Website":anchorList['Website']},
                            {"$addToSet" :
                                    {"SiteURL":{"$each" :anchorList['SiteURL']} }  }
                                       )                               
        post.update({"Website":anchorList['Website']},
                            {"$set" :
                                    {"BaseUrl":anchorList['BaseUrl'] }  },
                                    upsert=True
                                       )                               
    logger.debug("All Anchor's Tag are updated in Mongo Successfully...")
    print("New Relative Anchor's Added Success")


def getRelativeLinksFromDB():
    AllLinks=[]
    db=establishMongoConnection()
    posts = db.urls
    data=posts.find({"Website":"thelogicalindian"})
    for links in data:
        AllLinks.append(links)
    return AllLinks

def finalStoringofAnchorsInDB(data):
    db=establishMongoConnection()
    logger.debug("Mongo Connection Establish Successfully...")
    Anchor_Tag_List =data
    post=db.urls
    for anchorList in Anchor_Tag_List:
        post.update({"Website":anchorList['Website']},
                            {"$addToSet" :
                                    {"SiteURL":{"$each" :anchorList['SiteURL']} }  }
                                       )
    logger.debug("All Anchor's Tag are updated in Mongo Successfully...")
    print("New Anchor's Added Success")
        
        
    


listofLinksData=[]


def fetchingURLsList():
    db=establishMongoConnection()
    posts = db.urls
    filteredJSON=posts.aggregate([{
        "$project":{"Website":1,"SelectivesInfo":1,"RelativeLinks":1,"BaseUrl":1,"SiteURL":{"$setUnion": [ "$SiteURL", [] ] } }} ])
    for jsonData in filteredJSON:
        listofLinksData.append(jsonData)
    
    return listofLinksData 
#fetchingURLsList()    
  


def StoringSelectivesInfoInURLs_Schema(objects):
    db=establishMongoConnection()
    logger.debug("Mongo Connection Established Successfully for storing Selectives info in urlSchema...")
    SelectiveInfo =objects
    post=db.urls
    
    for ListInfo in SelectiveInfo:
        SavedUrls=post.update_one({
            "Website":ListInfo['siteName']
        },
        {   "$set":{"SelectivesInfo":ListInfo['selectives']
        }})
    logger.debug("Selectives are stored in Mongo Successfully...")
    return "success"


def filterArticlesBody(text):                       
    tag_re = re.compile(r'(<!--.*?-->|<[^>]*>)')
    no_tags = tag_re.sub('', text)
    ready_for_web = cgi.escape(no_tags)
    sentence = ready_for_web
    scentence =" ".join(sentence.split())
    return scentence    

def storingScrappedItemsOfArticlesinDB(websitename,url,title,Date,author,category,Content,Image,Tags,BaseUrl,TimeStamp):
    db=establishMongoConnection()
    logger.debug("Mongo Connection Established Successfully for storing all Scrapped Materials into DB..")
    post=db.updatedarticles
    content=filterArticlesBody(Content)
    obj={          "urlsInfo":{                  "BaseUrl" : BaseUrl,
                                                 "urlName":url,
                                                 "Title":title,
                                                 "Date":Date,
                                                 "TimeStamp" : TimeStamp,
                                                 "Author":author,
                                                 "Category":category,
                                                 "Content":content,
                                                 "ImageSource":Image,
                                                 "Tags":Tags,
                                                 "CrawledAt":datetime.datetime.strptime(now.strftime("%Y-%m-%dT%H:%M"),"%Y-%m-%dT%H:%M")
                                    }                 
                        }
    post.update({"WebInfo.urlsInfo.Title":title},
           {"$setOnInsert":{
             "WebSite":websitename, 
             "WebInfo":obj
            }},
             upsert=True
          )    
    logger.debug("All Scrapped Items for "+""+websitename+""+url+""+" stored in Mongo Successfully...")
    return "Stored in DB..."

def getArticlesBodyForExtractNoun():
    listofArticlesDetails=[]
    db=establishMongoConnection()
    logger.debug("Mongo Connection Established Successfully for Fetching all Articles Content from DB..")
    posts = db.updatedarticles
    cursor = posts.find({
                        "WebInfo.urlsInfo.Content":{"$exists":True}})            
    for alldocs in cursor:
        listofArticlesDetails.append(alldocs)  
    return listofArticlesDetails 


def storingNounsCorrespodingToArticles(nounlist):
    NounsArray=nounlist
    db=establishMongoConnection()
    logger.debug("Mongo Connection Established Successfully for storing Nouns List in DB...")
    post=db.updatedarticles
    #print(NounsArray)
    
    for i in NounsArray:
        post.update_one({ "WebInfo.urlsInfo.urlName":i['urlName']
                                           },
                              {"$set":{ "WebInfo.urlsInfo.Nouns":i['NounsList'] }}
             )
    logger.debug("Nouns are stored in Mongo Successfully...")
    print("Nouns are Stored..")
    
    return "success"

        
def getJSONFromDBForSentimentTest():
    listofAllArticles=[]
    db=establishMongoConnection()
    logger.debug("Mongo Connection Established Successfully for Fetching all Articles Details from DB..")
    posts = db.updatedarticles
    cursor = posts.find({"WebInfo.urlsInfo.Title":{"$exists":True}})           
    for alldocs in cursor:
        listofAllArticles.append(alldocs)  
    return listofAllArticles


def sentimentsForTitle_DB(data):
    sentiments=data
    db=establishMongoConnection()
    logger.debug("Mongo Connection Established Successfully for storing Sentiments for Title List DB...")
    posts = db.updatedarticles

    for i in sentiments:
        posts.update({ "WebInfo.urlsInfo.Title":i['Title']
                                           },
                              {"$set":{
                                          "WebInfo.urlsInfo.Sentiments.Title.neg":i['Sentiment']['neg'],
                                          "WebInfo.urlsInfo.Sentiments.Title.neu":i['Sentiment']['neu'] ,
                                          "WebInfo.urlsInfo.Sentiments.Title.pos":i['Sentiment']['pos'] ,
                                          "WebInfo.urlsInfo.Sentiments.Title.compound":i['Sentiment']['compound']  
                                          
                                          }
                               },
                                upsert=True
             )
        logger.debug("Sentiments for title "+""+i['Title'] +""+"are stored in Mongo Successfully...")
    print("Sentiments for title are Stored......")

    return "success"


def sentimentsForScentences_DB(data):
    sentiments=data
    db=establishMongoConnection()
    logger.debug("Mongo Connection Established Successfully for storing Sentiments for Scentences corresponding to Articles in DB...")
    posts = db.updatedarticles
    
    for i in sentiments: 
        posts.update({ "WebInfo.urlsInfo.Title":i['Title']
                                           },
                              {"$push":{   "WebInfo.urlsInfo.Sentiments.Scentences":{"$each":[{ "Scentence":i['Sentimentals']['Scentence'],
                                                                            "SentimentLevel":{
                                                                                  "neg":i['Sentimentals']['Sentiment']['neg'],
                                                                                  "neu":i['Sentimentals']['Sentiment']['neu'],
                                                                                  "pos":i['Sentimentals']['Sentiment']['pos'],
                                                                            "compound":i['Sentimentals']['Sentiment']['compound']
                                                         }}] }

                                                                                  }
                              })
    logger.debug("Sentiments for Scentences are Stored Successfully...")
    print("Sentiments for Scentences are Stored..")
    return "success"    


def sentimentsForArticles_DB(data):
    sentiments=data
    db=establishMongoConnection()
    logger.debug("Mongo Connection Established Successfully for storing Sentiments for Article corresponding to Articles in DB...")
    posts = db.updatedarticles
    for i in sentiments:
        posts.update_one({ "WebInfo.urlsInfo.Title":i['Title']
                                           },
                              {"$set":{
                                          "WebInfo.urlsInfo.Sentiments.Article.neg":i['Sentimentals']['Sentiment']['neg'],
                                          "WebInfo.urlsInfo.Sentiments.Article.neu":i['Sentimentals']['Sentiment']['neu'] ,
                                          "WebInfo.urlsInfo.Sentiments.Article.pos":i['Sentimentals']['Sentiment']['pos'] ,
                                          "WebInfo.urlsInfo.Sentiments.Article.compound":i['Sentimentals']['Sentiment']['compound']  
                                          
                                          }
                              
                               }
             )
        logger.debug("Sentiments for Articles are Stored successfully in DB...")
    


    print("Sentiments for Articles are Stored....")
    return "success" 


def getArticlesDataForTF():
    listdata=[]
    db=establishMongoConnection()
    posts = db.updatedarticles
    cursor=posts.find({"WebInfo.urlsInfo.tfidf":{"$exists":False},
			"WebInfo.urlsInfo.Content":{"$exists":True},
            "WebInfo.urlsInfo.tfInfo.tf":{"$exists":False}})
    for doc in cursor:
        listdata.append(doc)
    return listdata

def getArticlesDataForIDF():
    listdata=[]
    db=establishMongoConnection()
    posts = db.updatedarticles
    cursor=posts.find({"WebInfo.urlsInfo.tfInfo.tf":{"$exists":True}})
    for doc in cursor:
        listdata.append(doc)
    return listdata    
        
def storeTFInDB(data):
    db=establishMongoConnection()
    posts = db.updatedarticles
    for doc in data:
        obj={
            
                                                   "tf":doc['tf'],
                                                    "flags":doc['flag'],
                                                   "terms":doc['terms'],
                                                   "tag" :"default"      
                        }
        posts.update({'WebInfo.urlsInfo.Title':doc['title']},
                            {'$set': {"WebInfo.urlsInfo.tfInfo":obj}},
                                 upsert=True
                                )
    print("Success")

def storeTF_IDF(data):
    db=establishMongoConnection()
    posts = db.updatedarticles
    for doc in data:
        obj={
                           "idf":doc['idf'],
                           "tfIdf":doc['tfIdf'],
                           "flags":doc['flag']
             }
        posts.update({'WebInfo.urlsInfo.Title':doc['title']},
                           {'$set': {"WebInfo.urlsInfo.tfInfo":obj}},
                           upsert=True
               
            )
    print("All IDF is Stored....")          
    
    
def getTF_IDFFromDB():
    db=establishMongoConnection()
    posts = db.updatedarticles

    data = posts.find({"WebInfo.urlsInfo.tfInfo.tfIdf":{"$exists":True}},
                  {"WebInfo.urlsInfo.tfInfo":1,"WebInfo.urlsInfo.Title":1})

    return data

def send_For_Storage(data):
    db=establishMongoConnection()
    posts = db.updatedarticles
    for doc in data:
        posts.update({'WebInfo.urlsInfo.Title':doc['Title']},
                           {'$set': {"WebInfo.urlsInfo.TopIdf":doc['TermsName']}},
                           upsert=True
               
             )
    print(" top IDf is stored..")

def updateTfIdfDocInDB(data):
    db=establishMongoConnection()
    posts = db.updatedarticles
    
    for doc in data:
        posts.update({'WebInfo.urlsInfo.Title':doc['Title']}, { "$unset" : { "WebInfo.urlsInfo.tfInfo" : 1} })
        posts.update({'WebInfo.urlsInfo.Title':doc['Title']},
                           {'$set': {"WebInfo.urlsInfo.tfidf":doc['SortedTerms']}},
                           upsert=True
               
             )
    print(" IDf is stored..")

    



   
