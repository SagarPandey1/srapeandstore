from bson import json_util
from bs4 import BeautifulSoup
from nltk.tag import pos_tag
from db1 import fetchingURLsList
from db1 import storeFirstUrl
from db1 import storeSecondUrl
from db1 import fetchingFirstLevelUrlFromDB
from db1 import fetchingSecondLevelUrlFromDB
from db1 import storeThirdUrl
import urllib2
import re
import time

from logHandling import logSettings
logger=logSettings()




#Header's Information....
user_agent = '5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36'
headers = { 'User-Agent' : user_agent }          
data = None


#Stroring all links in Array
AllLinksArr=[]



        

def getRe_Request1(obj):
    #Sending second Requests....
    
    request0 = urllib2.Request(obj, data, headers)


    #Fetching Information from all Sites..
    res0 =urllib2.urlopen(request0) 

    if res0.getcode() == 200:
        page0=res0.read()
        logger.debug("Page Reading From indianKanoon Server is done")
    else:
        logger.debug("Problem is occur while Reading the WebPage indianKanoon.com")

    #soup Applied.....
    soup0 = BeautifulSoup(page0, "html5lib")


    #Scrapping all Anchor's Tags from all URL's.....
    data0 = soup0.findAll('div', attrs = {'class':"browselist"})
    linksArr0=[]
    if data0:
        for div in data0:
            
            links = div.findAll('a', attrs={'href': re.compile("/")})
            for a in links:
                linksArr0.append("https://indiankanoon.org"+a.get('href'))                                        
        urlList0= linksArr0
        AllLinksArr.append({  "WebSiteName":"indiankanoon",
                            "urlName":obj,
                            "UriFirst":urlList0
                     
        })

        #logger.debug("Anchors Tag Scrapping from   is done Successfully...")
        print(AllLinksArr)
    else:
        print("Nothing is found...")
        #logger.debug("Nothing is find from thenextweb.com please check tags or their class or id Names specified in soup.findAll()")    


 #---------------------------------------Request 2-----------------------------------       
NextLinks=[]

def getRe_Request2():
    level1urls= fetchingFirstLevelUrlFromDB()       #fetching all level1 URLs
    
    for urlsItem in level1urls:
        print("Crawling for Level 2 uri's is in progress....")
        for level1url in urlsItem['RelatedURL']['FirstUrl']:
            linksOne=[]
            for uri in level1url['links']:
        
                request0 = urllib2.Request(uri, data, headers)

                try:
                    res0 =urllib2.urlopen(request0) 
                except urllib2.HTTPError as err:
                    if res0.getcode() == 503:
                        continue 
                if res0.getcode() == 200:
                    page0=res0.read()
                    
                    logger.debug("Page Reading From indianKanoon Server is done")
                
                else:
                    logger.debug("Problem is occur while Reading the WebPage indianKanoon.com")

                soup0 = BeautifulSoup(page0, "html5lib")
                
                data0 = soup0.findAll('div', attrs = {'class':"browselist"})
                linksArr0=[]
                if data0:
                    for div in data0:
                        links = div.findAll('a', attrs={'href': re.compile("/")})
                        for a in links:
                            linksArr0.append("https://indiankanoon.org"+a.get('href'))                                        
                    urlList0= linksArr0
                    NextLinks.append({  "WebSiteName":"indiankanoon",
                                          "urlName":uri,
                                          "UriSec":urlList0
                     
                     })
            print(NextLinks)

    print("Crawling for level 2 uri's done and Now Send for Storing in DB...")
    storeSecondUrl(data=NextLinks)  

LastLinks=[]

def getRe_Request3():
    level2urls= fetchingSecondLevelUrlFromDB()     #fetching all level2 URLs
    for urlsItem in level2urls:
        for urlsInfo in urlsItem['RelatedURL']['SecondUrl']:
            for actUrl in urlsInfo['links']:
                linksOne=[]
        
                request0 = urllib2.Request(actUrl, data, headers)

                try:
                    res0 =urllib2.urlopen(request0) 
                except urllib2.HTTPError as err:
                    if res0.getcode() == 503:
                        continue 
                if res0.getcode() == 200:
                    page0=res0.read()
                    
                    logger.debug("Page Reading From indianKanoon Server is done")
                
                else:
                    logger.debug("Problem is occur while Reading the WebPage indianKanoon.com")

                soup0 = BeautifulSoup(page0, "html5lib")
                
                data0 = soup0.findAll('div', attrs = {'class':"result"})
                linksArr0=[]
                if data0:
                    for div in data0:
                        links = div.findAll('a', attrs={'href': re.compile("/")})
                        for a in links:
                            linksArr0.append("https://indiankanoon.org"+a.get('href'))                                        
                    urlList0= linksArr0
                    LastLinks.append({  "WebSiteName":"indiankanoon",
                                          "urlName":actUrl,
                                          "UriSec":urlList0
                     
                     })
            print(LastLinks)
            storeThirdUrl(data=LastLinks)
    print("Crawling for level 3 uri's done and Now Send for Storing in DB...")        
                
                      
    
    
               

def fetchAll_URL():
    data=fetchingURLsList()
    for doc in data:
        for url in doc['SiteURL']:
            print(url)
            getRe_Request1(obj=url)
    storeFirstUrl(data=AllLinksArr)  

   


        

fetchAll_URL()      
getRe_Request2()  
getRe_Request3()

   
   