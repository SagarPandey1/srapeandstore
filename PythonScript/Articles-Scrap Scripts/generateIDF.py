# -*- coding: utf-8 -*-
# own files and libraries

import time
import math

from db import getArticlesDataForIDF
from db1 import getKanoonData
from db1 import storeTFIDF
from db import establishMongoConnection
from db import storeTF_IDF

print("TF-IDF Calculation is in progress....")

articles = getArticlesDataForIDF()

db = establishMongoConnection()
posts = db.updatedarticles





for article in articles:
	idf = {}
	tfIdf = {}
	allTfIdf = []
	for term in article['WebInfo']['urlsInfo']['tfInfo']['terms']:
		# getting all docs in same domain (tags)
		totalDocs = posts.find(
		    {'WebInfo.urlsInfo.tfInfo.tag': article['WebInfo']['urlsInfo']['tfInfo']['tag']}).count()
		# getting all docs with the term in same domain (tags)
		termDocs = posts.find({'WebInfo.urlsInfo.tfInfo.terms': term,
		                      'WebInfo.urlsInfo.tfInfo.tag': article['WebInfo']['urlsInfo']['tfInfo']['tag']}).count()

		idf[term] = math.log(totalDocs/termDocs)
		# calculating tf_idf
		tfIdf[term] = idf[term] * article['WebInfo']['urlsInfo']['tfInfo']['tf'][term]


	allTfIdf.append({     "title":article['WebInfo']['urlsInfo']['Title'],
		                 "idf":idf,
						 "tfIdf":tfIdf,
						 "flag":1
	            })

	storeTF_IDF(data=allTfIdf)
'''

###---------------TF-IDF Calculation for IndianKanooon------

cases = getKanoonData()

db = establishMongoConnection()
posts = db.cases





for article in cases:
	idf = {}
	tfIdf = {}
	TfIdfList = []
	for term in article['WebInfo']['urlsInfo']['tfInfo']['terms']:
		# getting all docs in same domain (tags)
		totalDocs = posts.find(
		    {'WebInfo.urlsInfo.tfInfo.tag': article['WebInfo']['urlsInfo']['tfInfo']['tag']}).count()
		# getting all docs with the term in same domain (tags)
		termDocs = posts.find({'WebInfo.urlsInfo.tfInfo.terms': term,
		                      'WebInfo.urlsInfo.tfInfo.tag': article['WebInfo']['urlsInfo']['tfInfo']['tag']}).count()

		idf[term] = math.log(totalDocs/termDocs)
		# calculating tf_idf
		tfIdf[term] = idf[term] * article['WebInfo']['urlsInfo']['tfInfo']['tf'][term]


	TfIdfList.append({     "title":article['WebInfo']['urlsInfo']['Title'],
		                 "idf":idf,
						 "tfIdf":tfIdf,
						 "flag":1
	            })
	storeTFIDF(data=TfIdfList) 

'''


