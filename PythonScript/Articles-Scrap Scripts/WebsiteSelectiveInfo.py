from db import StoringSelectivesInfoInURLs_Schema
from logHandling import logSettings
logger=logSettings()


def webSiteSelective():
    AttrInfo=[]
    webSiteName="mashable"
    selectiveInfo = {   
		
              "Category" : {
			"ClassName" : "channel page-header post-head",
			"SelectiveName" : "hgroup",
			"innerElement" : ""
		},
		"ArticleBody" : {
			"ClassName" : "post-content",
			"getClassName" : "",
			"SelectiveName" : "div",
			"innerElement" : ""
		},
		"Title" : {
			"ClasName" : "title",
			"SelectiveName" : "h1",
			"innerElement" : ""
		},
		"Image" : {
			"ClassName" : "microcontent",
			"SelectiveName" : "img"
		},
		"Author" : {
			"ClassName" : "author_name",
			"SelectiveName" : "span",
			"innerElement" : ""
		},
		"Date" : {
			"ClassName" : "article-info",
			"SelectiveName" : "div",
			"innerElement" : "time",
			"dateKey" : "",
			"dateFormat4" : "",
			"dateFormat1" : "%Y-%m-%d %H:%M:%S UTC",
			"dateFormat2" : "%Y-%m-%d %H:%M:%S %z",
			"dateFormat3" : "%Y-%m-%d %I:%M:%S %z"
		},
		"Tags" : {
			"ClassName" : "article-topics",
			"SelectiveName" : "footer",
		
         }

	}
    AttrInfo.append({
                   "siteName":webSiteName,
                   "selectives":selectiveInfo
    })
    
    result=StoringSelectivesInfoInURLs_Schema(objects=AttrInfo)
    if(result=="success"):
        logger.debug("Selective Deatils for"+""+webSiteName+""+"WebSite Successfully Stored in DB...")
    else:
        logger.debug("Some problem is occur While Storing Selective Information of "+""+webSiteName+""+"WebSite")    

webSiteSelective()              