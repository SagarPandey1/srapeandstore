from db1 import StoreSelectors
from logHandling import logSettings
logger=logSettings()


def webSiteSelective():
    AttrInfo=[]
    webSiteName="indianKannon"
    selectiveInfo={			"Date" : {
			"ClassName" : "",
			"SelectiveName" : "span",
			"getDate" : ""
		},
		"Category" : {
			"ClassName" : "",
			"SelectiveName" : "a"
		},
		"Author" : {
			"ClassName" : "doc_author",
			"SelectiveName" : "div"
		},
		"ArticleBody" : {
			"SelectiveName" : "div",
			"getClassNameForScience" : "",
			"getClassNameForBusiness" : "",
			"getClassNameForBackChannel" : "",
			"ClassName" : "judgments",
			"getClassNameForCulture" : ""
		},
		"Title" : {
			"ClasName" : "doc_title",
			"SelectiveName" : "div"
		}

                  
                 }
    AttrInfo.append({
                   "siteName":webSiteName,
                   "selectives":selectiveInfo
    })
    
    result=StoreSelectors(objects=AttrInfo)
    if(result=="success"):
        logger.debug("Selective Deatils for"+""+webSiteName+""+"WebSite Successfully Stored in DB...")
    else:
        logger.debug("Some problem is occur While Storing Selective Information of "+""+webSiteName+""+"WebSite")    

webSiteSelective()