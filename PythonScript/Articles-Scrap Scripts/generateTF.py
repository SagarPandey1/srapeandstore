
import time, re
from db import getArticlesDataForTF
from db1 import getKanoonData
from db1 import storeTFofKanoonInDB
from db import storeTFInDB



articles= getArticlesDataForTF()




for article in articles:
    	terms = re.sub('[^a-zA-Z0-9\s]', '', article['WebInfo']['urlsInfo']['Content'].lower()).split(' ')
        counter = {}
        alltf=[]
        for term in terms:
    			
    			if term in counter:
    					counter[term] += 1
		        else:
    					counter[term] = 1
        tf={}
        for term, count in counter.iteritems():
    			tf[term] = float(count)/float(len(terms))
        alltf.append(
			      {
			    "title":article['WebInfo']['urlsInfo']['Title'],
				"tf":tf,
				"terms":counter.keys(),
				"flag":0
		    })
        storeTFInDB(data=alltf)


########   for indian Kanoon Tf generator-----------------

data=getKanoonData()

'''

for article in data:
    	terms = re.sub('[^a-zA-Z0-9\s]', '', article['WebInfo']['urlsInfo']['Content'].lower()).split(' ')
        counter = {}
        alltfforCases=[]
        for term in terms:
    			
    			if term in counter:
    					counter[term] += 1
		        else:
    					counter[term] = 1
        tf={}
        for term, count in counter.iteritems():
    			tf[term] = float(count)/float(len(terms))
        alltfforCases.append(
			      {
			    "title":article['WebInfo']['urlsInfo']['Title'],
				"tf":tf,
				"terms":counter.keys(),
				"flag":0
		    })				
        storeTFofKanoonInDB(data=alltfforCases)


'''		