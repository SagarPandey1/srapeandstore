from bs4 import BeautifulSoup
from db import storingAnchorTags
from db import UpdatingAnchorTags
from db import fetchingURLsList
import urllib2
import re
import time

from logHandling import logSettings
logger=logSettings()



# URL's list.....
'''
url0='https://thenextweb.com'
url1='https://www.wired.com'
url2='https://mashable.com'
url3='http://www.firstpost.com/tech'
url4='https://gizmodo.com/c/space'
url6='https://yourstory.com'
'''
url5='https://indiankanoon.org/browse/'

#Header's Information....
user_agent = '5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36'
headers = { 'User-Agent' : user_agent }          
data = None


#Stroring all links in Array
AllLinksArr=[]
linksArr0=[]
linksArr1=[]
linksArr2=[]
linksArr3=[]
linksArr4=[]
linksArr5=[]
linksArr6=[]


def getRequest():                  
    #Sending Requests....
    '''
    request0 = urllib2.Request(url0, data, headers)
    request1 = urllib2.Request(url1, data, headers)
    request2 = urllib2.Request(url2, data, headers)
    request3 = urllib2.Request(url3, data, headers)
    request4 = urllib2.Request(url4, data, headers)
    request6 = urllib2.Request(url6, data, headers)
    '''
    request5 = urllib2.Request(url5, data, headers)
    
    
    '''
    #Fetching Information from all Sites..
    res0 =urllib2.urlopen(request0)  
    res1 =urllib2.urlopen(request1)
    res2 =urllib2.urlopen(request2)  
    res3 =urllib2.urlopen(request3)
    res4 =urllib2.urlopen(request4)
    res6=urllib2.urlopen(request6)
    '''
    res5=urllib2.urlopen(request5)
    
    '''
    if res0.getcode() == 200:
        page0=res0.read()
        logger.debug("Page Reading From thenextweb.com Server is done")
    else:
        logger.debug("Problem is occur while Reading the WebPage thenextweb.com")
    
    if res1.getcode() == 200:
        page1=res1.read()
        logger.debug("Page Reading From wired.com Server is done")
    else:
        logger.debug("Problem is occur while Reading the WebPage wired.com")
    
    if res2.getcode() == 200:
        page2=res2.read()
        logger.debug("Page Reading From mashable.com Server is done")
    else:
        logger.debug("Problem occur while Reading the WebPage mashable.com")
    
    
    if res3.getcode() == 200:
        page3=res3.read()
        logger.debug("Page Reading From firstpost.com Server is done")
    else:
        logger.debug("Problem occur while Reading the WebPage firstpost.com")
   
    if res4.getcode() == 200:
        page4=res4.read()
        logger.debug("Page Reading From gizmodo Server is done")
    else:
        logger.debug("Problem occur while Reading the WebPage gizmodo.com")    
    '''
    if res5.getcode() == 200:
        page5=res5.read()
        logger.debug("Page Reading From theLogicalIndian Server is done")
    else:
        logger.debug("Problem occur while Reading the WebPage thelogicalindian.com")
    '''
    if res6.getcode() == 200:
        page6=res6.read()
        logger.debug("Page Reading From yourStory Server is done")
    else:
        logger.debug("Problem occur while Reading the WebPage yourStory.com") 
    '''      

    
    #soup0 = BeautifulSoup(page0, "html5lib")
    #soup1 = BeautifulSoup(page1, "html5lib")  
    #soup2 = BeautifulSoup(page2, "html5lib")    
    #soup3 = BeautifulSoup(page3, "html5lib")    
    #soup4 = BeautifulSoup(page4, "html5lib")
    soup5 = BeautifulSoup(page5, "html5lib")
    #soup6 = BeautifulSoup(page6, "html5lib")


    

#Scrapping all Anchor's Tags from all URL's.....

    
#finding the All  Anchor tags From the thenextweb.com....
    '''
    data0 = soup0.findAll('main', attrs = {'class':"main"})
    if data0:
        for div in data0:
            links = div.findAll('a', attrs={'href': re.compile("^https://")})
            for a in links:
                linksArr0.append(a.get('href'))                                        
        urlList0= list(set(linksArr0))
        AllLinksArr.append({ "WebSiteName":"thenextweb",
                              "AllURLs":urlList0

        })
             
        logger.debug(urlList0)
        logger.debug("Anchors Tag Scrapping from thenextweb.com  is done Successfully...")
    else:
        logger.debug("Nothing is find from thenextweb.com please check tags or their class or id Names specified in soup.findAll()")    
        #print("Nothing is Found")
    #print(AllLinksArr) 
           

#finding the All  Anchor tags From the wired.com....
    data1 = soup1.findAll('div', attrs = {'class':"homepage-main"})
    if data1:
        for div in data1:
            links = div.findAll('a', attrs={'href': re.compile("/")})
            for a in links:
                linksArr1.append('https://www.wired.com'+a.get('href'))                                        
        urlList1= list(set(linksArr1))
        AllLinksArr.append({  "WebSiteName":"wired",
                              "AllURLs":linksArr1

        })
        logger.debug("Anchors Tag Scrapping from wired.com  is done Successfully...")
    else:
        #print("Nothing is Found...")
        logger.debug("Nothing is find from wired.com please check tags or their class or id Names specified in soup.findAll()")
    
    #print(AllLinksArr)    
       

#finding the All  Anchor tags From the mashable.com....
    data2 = soup2.findAll('div', attrs = {'class':"flex-box columns"})
    if data2:
        for div in data2:
            links = div.findAll('a', attrs={'href': re.compile("/")})
            for a in links:
                linksArr2.append('https://mashable.com'+a.get('href'))                                        
        urlList2= list(set(linksArr2))
        AllLinksArr.append({  "WebSiteName":"mashable",
                              "AllURLs":urlList2

        })
        #logger.debug(urlList2)
        logger.debug("Anchors Tag Scrapping from mashable.com  is done Successfully...")
    else:
        logger.debug("Nothing is find from mashable.com please check tags or their class or id Names specified in soup.findAll()")
    #print(AllLinksArr)

#finding the All  Anchor tags From the firstpost.com....
    data3 = soup3.findAll('div', attrs = {'class':"main"})
    if data3:
        for div in data3:
            links = div.findAll('a', attrs={'href': re.compile("^http://")})
            for a in links:
                linksArr3.append(a.get('href'))                                        
        urlList3= list(set(linksArr3))
        AllLinksArr.append({ "WebSiteName":"firstpost",
                              "AllURLs":urlList3
        }) 
        logger.debug(urlList3)
        logger.debug("Anchors Tag Scrapping from firstpost.com  is done Successfully...")
    else:
        #print("Nothing is founds")
        logger.debug("Nothing is find from firstpost.com please check tags or their class or id Names specified in soup.findAll()")
    #print(AllLinksArr)

##finding the All  Anchor tags From the gizmodo.com....
    data4 = soup4.findAll('section', attrs = {'class':"main"})
    if data4:
        for div in data4:
            links = div.findAll('a', attrs={'href': re.compile("^https://")})
            for a in links:
                linksArr4.append(a.get('href'))                                        
        urlList4= list(set(linksArr4))
        AllLinksArr.append({  "WebSiteName":"gizmodo",
                              "AllURLs":urlList4
                     
        })
        #logger.debug(urlList4)
        logger.debug("Anchors Tag Scrapping from gizmodo.com is done Successfully...")
    else:
        logger.debug("Nothing is find from gizmodo.com please check tags or their class or id Names specified in soup.findAll()")    
    #print(AllLinksArr)
     '''  
     
    data5 = soup5.findAll('div', attrs = {'class':"browselist"})
    if data5:
        for div in data5:
            links = div.findAll('a', attrs={'href': re.compile("/")})
            for a in links:
                linksArr5.append("https://indiankanoon.org"+a.get('href'))                                        
        urlList5= linksArr5
        AllLinksArr.append({  "WebSiteName":"indiankanoon",
                              "AllURLs":urlList5
                     
        })
          
        #logger.debug(urlList4)
        logger.debug("Anchors Tag Scrapping from thelogicalindian.com is done Successfully...")
    else:
        logger.debug("Nothing is find from thelogicalindian.com please check tags or their class or id Names specified in soup.findAll()") 
     
    '''  

    data6 = soup6.findAll('div', attrs = {'class':"show-grid row"})
    if data6:
        for div in data6:
            links = div.findAll('a', attrs={'href': re.compile("/")})
            for a in links:
                linksArr6.append('https://yourstory.com'+a.get('href'))                                        
        urlList6= list(set(linksArr6))
        AllLinksArr.append({  "WebSiteName":"yourstory",
                              "AllURLs":urlList6
                     
        })
        #logger.debug(urlList4)
        logger.debug("Anchors Tag Scrapping from yourstory.com is done Successfully...")
    else:
        logger.debug("Nothing is find from yourstory.com please check tags or their class or id Names specified in soup.findAll()")
    
    '''
    storingAnchorTags(data=AllLinksArr)    #This Method will Only run If we want to Add Some New Website Selectors  
    #UpdatingAnchorTags(data=AllLinksArr)    #This method is used to add New Anchors of Stored Website
    #time.sleep(300)
    print(AllLinksArr)
    return "Success"

#while True:
    #getRequest()    

    


'''
############### Find Anchors tag from Chain of links of a WebSite-----------------------

def getAnotherRequest():
    AllUrls=fetchingURLsList()
    for urls in AllUrls:
        
        if(urls['Website']=='thenextweb'):
            for siteurl in urls['SiteURL']:
                request0 = urllib2.Request(siteurl, data, headers)
                res0 =urllib2.urlopen(request0)
                if res0.getcode() == 200:
                    page0=res0.read()
                    logger.debug("Page Reading From thenextweb.com Server is done")
                else:
                    logger.debug("Problem is occur while Reading the WebPage thenextweb.com")
                soup0 = BeautifulSoup(page0, "html5lib")
                data0 = soup0.findAll('main', attrs = {'class':"main"})
                if data0:
                    for div in data0:
                        links = div.findAll('a', attrs={'href': re.compile("^https://")})
                        for a in links:
                            linksArr0.append(a.get('href'))                                        
                    urlList0= list(set(linksArr0))
                    AllLinksArr.append({ "WebSiteName":"thenextweb",
                              "AllURLs":urlList0

                    })
                    print(AllLinksArr)
                    logger.debug("Anchors Tag Scrapping from thenextweb.com  is done Successfully...")
                else:
                    logger.debug("Nothing is find from thenextweb.com please check tags or their class or id Names specified in soup.findAll()")    
        
        if(urls['Website']=='wired'):
            for siteurl in urls['SiteURL']:
                request0 = urllib2.Request(siteurl, data, headers)
                res0 =urllib2.urlopen(request0)
                if res0.getcode() == 200:
                    page0=res0.read()
                    logger.debug("Page Reading From thenextweb.com Server is done")
                else:
                    logger.debug("Problem is occur while Reading the WebPage thenextweb.com")
                soup0 = BeautifulSoup(page0, "html5lib")
                data1 = soup0.findAll('div', attrs = {'id':"app-root"})
                if data1:
                    for div in data1:
                        links = div.findAll('a', attrs={'href': re.compile("/")})
                        for a in links:
                            linksArr1.append('https://www.wired.com'+a.get('href'))                                        
                    urlList1= list(set(linksArr1))
                    AllLinksArr.append({  "WebSiteName":"wired",
                                "AllURLs":linksArr1

                            })
                    print(AllLinksArr)          
                    logger.debug("Anchors Tag Scrapping from wired.com  is done Successfully...")
                else:
                    logger.debug("Nothing is find from wired.com please check tags or their class or id Names specified in soup.findAll()")
'''
getRequest() 
     
#getAnotherRequest()                   

