from bs4 import BeautifulSoup
from db import fetchingURLsList
from db import storingScrappedItemsOfArticlesinDB
import urllib2
import datetime
import sys
import time
import calendar
reload(sys)
sys.setdefaultencoding('utf8')



# Header's Information....
user_agent = '5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36'


def traversingAllURLsFromDB():
    listofURLs=fetchingURLsList()
    #scrapArticlesContentsFromURLs(siteObject=listofURLs[7])
    for urlsDetails in listofURLs:
        scrapArticlesContentsFromURLs(siteObject=urlsDetails)


def scrapArticlesContentsFromURLs(siteObject):
    siteURL=siteObject['SiteURL']
    websiteName=siteObject['Website']
    baseurl=siteObject['BaseUrl']
    Selective_ArticleBody=siteObject['SelectivesInfo']['ArticleBody']['SelectiveName']
    Selective_Title=siteObject['SelectivesInfo']['Title']['SelectiveName']
    Selective_Date=siteObject['SelectivesInfo']['Date']['SelectiveName']
    Selective_Author=siteObject['SelectivesInfo']['Author']['SelectiveName']
    Selective_Category=siteObject['SelectivesInfo']['Category']['SelectiveName']
    Selective_Images=siteObject['SelectivesInfo']['Image']['SelectiveName']
    Selective_Tags=siteObject['SelectivesInfo']['Tags']['SelectiveName']

    InnerElem_Title = siteObject['SelectivesInfo']['Title']['innerElement']
    InnerElem_Date =  siteObject['SelectivesInfo']['Date']['innerElement']
    InnerElem_Author = siteObject['SelectivesInfo']['Author']['innerElement']
    InnerElem_Category = siteObject['SelectivesInfo']['Category']['innerElement']
    InnerElem_Tags = siteObject['SelectivesInfo']['Tags']['innerElement']

    Date_Format1 = siteObject['SelectivesInfo']['Date']['dateFormat1']
    Date_Format2 = siteObject['SelectivesInfo']['Date']['dateFormat2']
    Date_Format3 = siteObject['SelectivesInfo']['Date']['dateFormat3']
    Date_Format4 = siteObject['SelectivesInfo']['Date']['dateFormat4']

    datekey=""

    if(siteObject['SelectivesInfo']['Date']['dateKey']!=""):
        datekey = siteObject['SelectivesInfo']['Date']['dateKey']

    print("Scrapping of Article from "+websiteName+" "+"is in Progress")

    for site in siteURL:
        if(site.startswith(baseurl)):
            url=site    
            date=" "
            title=" "
            Content=" "
            content=" "
            Images=" "
            author=" "
            category=" "
            image=" "
            TimeStampDate=" "
            tags="dafault"
            ClassName_ArticleBody=siteObject['SelectivesInfo']['ArticleBody']['ClassName']
            ClassName_Title=siteObject['SelectivesInfo']['Title']['ClasName']
            ClassName_Date=siteObject['SelectivesInfo']['Date']['ClassName']
            ClassName_Author=siteObject['SelectivesInfo']['Author']['ClassName']
            ClassName_Category=siteObject['SelectivesInfo']['Category']['ClassName']
            ClassName_Images=siteObject['SelectivesInfo']['Image']['ClassName']
            ClassName_Tags=siteObject['SelectivesInfo']['Tags']['ClassName']
        
            try:
                opener = urllib2.build_opener()
                opener.addheaders = [('User-Agent', user_agent)]
                response = opener.open(url)
                pageDetails=response.read()
                Sitesoup = BeautifulSoup(pageDetails, "html5lib")
            
            except IOError:
                continue    
            print(url)
            print(" ")
            
            #-------- Conditions for Scrap Article Body -----------
            if(Sitesoup.find(Selective_ArticleBody,attrs={'class':ClassName_ArticleBody})):
                Content =Sitesoup.find(Selective_ArticleBody,attrs={'class':ClassName_ArticleBody}).findAll('p')
                for p in Content:
                        content+=''+p.text
            else:
                continue            
            
            #-------- Conditions for Scrap Title ------------
            if(Sitesoup.find(Selective_Title, attrs = {'class':ClassName_Title})!=None):       
                if(Sitesoup.find(Selective_Title, attrs = {'class':ClassName_Title}).find(InnerElem_Title)!=None):
                    Title = Sitesoup.find(Selective_Title, attrs = {'class':ClassName_Title}).find(InnerElem_Title) 
                else:
                    Title = Sitesoup.find(Selective_Title, attrs = {'class':ClassName_Title})
            else:
                Title=""        

            #-------- Conditions for Scrap Date -------------- 
            if(Sitesoup.find(Selective_Date, attrs = {'class':ClassName_Date})!=None):
                if(Sitesoup.find(Selective_Date, attrs = {'class':ClassName_Date}).find(InnerElem_Date)!=None):
                    Date = Sitesoup.find(Selective_Date, attrs = {'class':ClassName_Date}).find(InnerElem_Date)
                else:
                    Date = Sitesoup.find(Selective_Date, attrs = {'class':ClassName_Date})
            else:
                Date =None
            #-------- Conditions for Scrap Author Name -------------
            if(Sitesoup.find(Selective_Author, attrs = {'class':ClassName_Author})!=None):
                if(Sitesoup.find(Selective_Author, attrs = {'class':ClassName_Author}).find(InnerElem_Author)!=None):
                    Author =Sitesoup.find(Selective_Author, attrs = {'class':ClassName_Author}).find(InnerElem_Author)
                else:
                    Author =Sitesoup.find(Selective_Author, attrs = {'class':ClassName_Author})
            else:
                Author =None     
                

            #--------- Conditions for Scrap Category --------------        
            if(Sitesoup.find(Selective_Category, attrs = {'class':ClassName_Category})!=None):
                if(Sitesoup.find(Selective_Category, attrs = {'class':ClassName_Category}).find(InnerElem_Category)!=None):
                    Category =Sitesoup.find(Selective_Category, attrs = {'class':ClassName_Category}).find(InnerElem_Category)
                else:
                    Category =Sitesoup.find(Selective_Category, attrs = {'class':ClassName_Category})   
            else:
                Category =None 
            

            #--------- Conditions for Scrap Image---------------
            if(Sitesoup.find(Selective_Images, attrs = {'class':ClassName_Images})!=None):
                    if(Sitesoup.find(Selective_Images, attrs = {'class':ClassName_Images}).findChildren('img')!=[]):
                        Images =Sitesoup.find(Selective_Images, attrs = {'class':ClassName_Images}).findChildren('img')
                        #print(Sitesoup.find(Selective_Images, attrs = {'class':ClassName_Images}))   
                        for img in Images:
                            Images=img.get('src')
                    else:
                        Images =Sitesoup.find(Selective_Images, attrs = {'class':ClassName_Images})
                        Images = Images.get('src')
                
                            
            
            #---------- Conditions for Scrap Tags---------------
            if(Sitesoup.find(Selective_Tags, attrs = {'class':ClassName_Tags})!=None):
                if(Sitesoup.find(Selective_Tags, attrs = {'class':ClassName_Tags}).find(InnerElem_Tags)!=None):
                    Tags =Sitesoup.find(Selective_Tags, attrs = {'class':ClassName_Tags}).find(InnerElem_Tags)     
                else:
                    Tags =Sitesoup.find(Selective_Tags, attrs = {'class':ClassName_Tags})
            else:
                Tags= None      
                   
            if(content==" " or Content==None):
                continue
            if(Title==" " or Content==None):
                continue    
            
            if(Title!=None):
                title=Title.text.strip()
            #------------- Conditions for date  --------------              
            if(Date!=None and Title!=None):       
                try:
                    date=Date.text.strip()
                    if(date!=""):
                        date = datetime.datetime.strptime(date, Date_Format1).strftime("%Y-%m-%d")
                        dateObject = datetime.datetime.strptime(date, "%Y-%m-%d")
                        TimeStampDate = int(time.mktime(datetime.datetime.strptime(date, "%Y-%m-%d").timetuple()))
                        
                    else:
                        if(datekey!=""):
                            date = Date[datekey]
                            date = datetime.datetime.strptime(date, Date_Format1).strftime("%Y-%m-%d")
                            dateObject = datetime.datetime.strptime(date, "%Y-%m-%d")
                            TimeStampDate = int(time.mktime(datetime.datetime.strptime(date, "%Y-%m-%d").timetuple()))
                            
                        else:
                            date =""    
            
                except ValueError:
                    try:
                        date = datetime.datetime.strptime(date, Date_Format2).strftime("%Y-%m-%d")
                        dateObject = datetime.datetime.strptime(date, "%Y-%m-%d")
                        TimeStampDate = int(time.mktime(datetime.datetime.strptime(date, "%Y-%m-%d").timetuple()))
                        
                    except ValueError:
                        try:
                            date = datetime.datetime.strptime(date, Date_Format3).strftime("%Y-%m-%d")
                            dateObject = datetime.datetime.strptime(date, "%Y-%m-%d")
                            TimeStampDate = int(time.mktime(datetime.datetime.strptime(date, "%Y-%m-%d").timetuple()))
                            
                        except ValueError:
                            try:
                                date = datetime.datetime.strptime(date, Date_Format4).strftime("%Y-%m-%d")
                                dateObject = datetime.datetime.strptime(date, "%Y-%m-%d")
                                TimeStampDate= int(time.mktime(datetime.datetime.strptime(date, "%Y-%m-%d").timetuple()))
                            except ValueError:
                                date = ""
                            
                        
                         
            if(Author!=None):
                author=Author.text.strip()
                
            if(Category!=None):
                if(Category!=""):
                    category=Category.text.strip()    
                
            if(Images!=None):
                image=Images
                
                     
            if(Tags!=None):
                tags=Tags.text.strip()
                
                           
        
            print("URL "+url)
            print("Title "+title)
            print("Date "+str(dateObject))
            print("TimeStamp "+str(TimeStampDate))
            print("Author "+author)
            print("Image "+image)
            print("Category "+category)
            print("Tags "+str(tags))
            print("Content "+content)
            print("baseUrl "+baseurl)
            print("")
            print(" ")
            storingScrappedItemsOfArticlesinDB(websitename=websiteName,url=url,title=title,Date=dateObject,
                                    author=author,category=category,Content=content,
                                    Image=image,Tags=tags,BaseUrl=baseurl,TimeStamp=TimeStampDate)                  
        else:
            continue

traversingAllURLsFromDB()
    