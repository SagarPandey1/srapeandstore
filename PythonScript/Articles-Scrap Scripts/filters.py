from db import getTF_IDFFromDB
from db import send_For_Storage
from db import updateTfIdfDocInDB

from db1 import getTFIDFFromDB
from db1 import storeTopTFIDF
from db1 import updateTFIDF

def filtered_tfIDf():
    data = getTF_IDFFromDB()
    
    
    for doc in data:
    	tfidf_val = doc['WebInfo']['urlsInfo']['tfInfo']['tfIdf']
        sorted_keys = sorted(tfidf_val, key=tfidf_val.get, reverse=True)
        allTerms=[]
        sortedTf_Idf=[]
        for r in sorted_keys:
            obj={
                   "TermsName":r,
                   "value":tfidf_val[r]
                }
            allTerms.append(obj)
        sortedTf_Idf.append({
                         "Title":doc['WebInfo']['urlsInfo']['Title'],
                         "SortedTerms":allTerms
        })
        Top_5_IDF(data=sortedTf_Idf) 
        updateTfIdfDocInDB(data=sortedTf_Idf)
          
    #print(sortedTf_Idf)	

def Top_5_IDF(data):
    top_terms=[]
    for doc in data:
        listofTerms=doc['SortedTerms']
        top_terms.append({
                        "TermsName":listofTerms[:5],
                        "Title":doc['Title']
        })  
    send_For_Storage(data=top_terms)  
      
    	





###########Get top 5 Tf-IDF for IndianKanoon----

def filteredtfIDf():
    data = getTFIDFFromDB()
    
    
    for doc in data:
    	tfidf_val = doc['WebInfo']['urlsInfo']['tfInfo']['tfIdf']
        sorted_keys = sorted(tfidf_val, key=tfidf_val.get, reverse=True)
        allTerms=[]
        sortedTf_Idf=[]
        for r in sorted_keys:
            obj={
                   "TermsName":r,
                   "value":tfidf_val[r]
                }
            allTerms.append(obj)
        sortedTf_Idf.append({
                         "Title":doc['WebInfo']['urlsInfo']['Title'],
                         "SortedTerms":allTerms
        })
        Top5IDF(data=sortedTf_Idf) 
        updateTFIDF(data=sortedTf_Idf)
          
    #print(sortedTf_Idf)	

def Top5IDF(data):
    top_terms=[]
    for doc in data:
        listofTerms=doc['SortedTerms']
        top_terms.append({
                        "TermsName":listofTerms[:5],
                        "Title":doc['Title']
        })  
    storeTopTFIDF(data=top_terms)  
      
    	
#filteredtfIDf()
filtered_tfIDf()
