# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
from nltk.tag import pos_tag
from db import getArticlesBodyForExtractNoun
from nltk.stem import WordNetLemmatizer 
lemmatizer = WordNetLemmatizer()  
from db import storingNounsCorrespodingToArticles
from db1 import fetchingIndianKanoonCasesFromDB
from db1 import storingNounsCorrespodingToCases
from logHandling import logSettings
logger=logSettings()
import sys
reload(sys)
sys.setdefaultencoding('utf8')



AllNounsList=[]

def extractArticleBodyFromJSON():
    articlesList=getArticlesBodyForExtractNoun()
    for key in articlesList:
        Article=key["WebInfo"]["urlsInfo"]["Content"]
        WebsiteUrl=key["WebInfo"]["urlsInfo"]["urlName"]
        extractAndFilter(article=Article,websiteurl=WebsiteUrl)
    #print(AllNounsList)       
    storingNounsCorrespodingToArticles(nounlist=AllNounsList)        #send Noun For Storage
    logger.debug("Nouns list after Filteration goes for Storing in DB...")  

def extractAndFilter(article,websiteurl):
    nounArray=[]
    
    str1=''.join( c for c in article.decode('utf-8').strip() if  c not in '<(\[?)]/>"''.“–,@:#;^â€—' )
    tagged_sent = pos_tag(str1.split())
    propernouns = [word for word,pos in tagged_sent if pos == 'NNP']          #get the proper nouns which always begin with capital letters  
    listofNouns = list(set(propernouns))
    for i  in listofNouns:
        nounArray.append(lemmatizer.lemmatize(i, pos="n"))

    AllNounsList.append({ "urlName":websiteurl,
                          "NounsList":nounArray

    })
    logger.debug("Noun Filteration is Done..")
    return    


#----------------------For IndianKanoon----------
NounsForCases=[]

def extractCasesofIndianKanoon():
    CasesList=fetchingIndianKanoonCasesFromDB()
    for key in CasesList:
        Article=key["WebInfo"]["urlsInfo"]["Content"]
        WebsiteUrl=key["WebInfo"]["urlsInfo"]["urlName"]
        extractAndFiltertheNoun(article=Article,websiteurl=WebsiteUrl)
    print(NounsForCases)       
    storingNounsCorrespodingToCases(nounlist=NounsForCases)        #send Noun For Storage
    logger.debug("Nouns list after Filteration goes for Storing in DB...")  
    


def extractAndFiltertheNoun(article,websiteurl):
    nounArray=[]
    
    str1=''.join( c for c in article.decode('utf-8').strip() if  c not in '<(\[?)]/>"''.“–,@:#;^â€—' )
    tagged_sent = pos_tag(str1.split())
    propernouns = [word for word,pos in tagged_sent if pos == 'NNP']          #get the proper nouns which always begin with capital letters  
    listofNouns = list(set(propernouns))
    for i  in listofNouns:
        nounArray.append(lemmatizer.lemmatize(i, pos="n"))
        print(lemmatizer.lemmatize(i, pos="n"))
    NounsForCases.append({ "urlName":websiteurl,
                          "NounsList":nounArray

    })
    logger.debug("Noun Filteration is Done..")
    return

    
    
extractArticleBodyFromJSON()   
#extractCasesofIndianKanoon() 