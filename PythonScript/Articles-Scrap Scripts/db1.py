from logHandling import logSettings
logger=logSettings()
import re ,cgi

from db import establishMongoConnection


def storeFirstUrl(data):
    db=establishMongoConnection()
    logger.debug("Mongo Connection Establish Successfully...")
    Anchor_Tag_List =data
    #print(Anchor_Tag_List)
    obj=[]
    post=db.relatedUrls
    for anchorList in Anchor_Tag_List:
        
        obj.append({
                        "urlName":anchorList['urlName'],
                        "links":anchorList['UriFirst']
        })

    SavedUrls=post.insert_one({
                              "Website":anchorList['WebSiteName'],
                             "RelatedURL":{     
                                                    "FirstUrl":obj
                                         }
                          
                           } )
    print("level 1 urls are Stored")

def storeSecondUrl(data):
    db=establishMongoConnection()
    logger.debug("Mongo Connection Establish Successfully...")
    Anchor_Tag_List =data
    #print(Anchor_Tag_List)
    obj=[]
    post=db.relatedUrls
    for anchorList in Anchor_Tag_List:
        obj.append({
                        "urlName":anchorList['urlName'],
                        "links":anchorList['UriSec']
        })
    SavedUrls=post.update({"Website":anchorList['WebSiteName']},
                                {"$set":{"RelatedURL.SecondUrl":obj}},
                                         upsert=True
                                 )
    print("level 2 urls are Stored")

def storeThirdUrl(data):
    db=establishMongoConnection()
    logger.debug("Mongo Connection Establish Successfully...")
    Anchor_Tag_List =data
    #print(Anchor_Tag_List)
    obj=[]
    post=db.relatedUrls
    for anchorList in Anchor_Tag_List:
        obj.append({
                        "urlName":anchorList['urlName'],
                        "links":anchorList['UriSec']
        })
    SavedUrls=post.update({"Website":anchorList['WebSiteName']},
                                {"$set":{"RelatedURL.ThirdUrl":obj}},
                                         upsert=True
                                 )
    print("level 3 urls are Stored")         

def fetchingFirstLevelUrlFromDB():
    listofLevel1Urls=[]
    db=establishMongoConnection()
    logger.debug("Mongo Connection Establish Successfully...")
    posts = db.relatedUrls
    cursor = posts.find()           
    for alldocs in cursor:
        listofLevel1Urls.append(alldocs)      
    return listofLevel1Urls

#fetchingFirstLevelUrlFromDB()

def fetchingSecondLevelUrlFromDB():
    listofLevel2Urls=[]
    db=establishMongoConnection()
    logger.debug("Mongo Connection Establish Successfully...")
    posts = db.relatedUrls
    cursor = posts.find({},{"RelatedURL.FirstUrl":0})           
    for alldocs in cursor:
        listofLevel2Urls.append(alldocs)   
    #print(listofLevel2Urls)       
    return listofLevel2Urls
#fetchingSecondLevelUrlFromDB()


def StoreSelectors(objects):
    db=establishMongoConnection()
    logger.debug("Mongo Connection Establish Successfully...")
    posts = db.relatedUrls
    SelectiveInfo=objects
    for ListInfo in SelectiveInfo:
        SavedUrls=posts.update({"Website":ListInfo['siteName']},
                                {   "$set":{"RelatedUrl.SelectivesInfo":ListInfo['selectives']
                                         }},
                                         upsert=True
                                         )

    logger.debug("Selectives are stored in Mongo Successfully...")
    return "success"

def fetchingLastLevelsofUrlFromDB():
    listofLevel3Urls=[]
    db=establishMongoConnection()
    logger.debug("Mongo Connection Establish Successfully...")
    posts = db.relatedUrls
    cursor = posts.find({},{"RelatedURL.FirstUrl":0,"RelatedUrl.SecondUrl":0})           
    for alldocs in cursor:
        listofLevel3Urls.append(alldocs)   
    #print(listofLevel3Urls)       
    return listofLevel3Urls

def filterArticlesBody(text):                       
    tag_re = re.compile(r'(<!--.*?-->|<[^>]*>)')
    no_tags = tag_re.sub('', text)
    ready_for_web = cgi.escape(no_tags)
    sentence = ready_for_web
    scentence =" ".join(sentence.split())
    return scentence     


def storingScrappedItemsOfArticlesinDB(websitename,url,title,Date,author,category,Content):
    db=establishMongoConnection()
    logger.debug("Mongo Connection Established Successfully for storing all Scrapped Materials into DB..")
    post=db.cases
    content=filterArticlesBody(Content)
    obj={          "urlsInfo":{
                                                 "urlName":url,
                                                 "Title":title,
                                                 "Date":Date,
                                                 "Author":author,
                                                 "Category":category,
                                                 "Content":content
                                    }                 
                        }
    SavedUrls=post.update({"WebInfo.urlsInfo.Title":title},
           {"$setOnInsert":{
             "WebSite":websitename,
             "WebInfo":obj
            }},
             upsert=True
          )  
    print(obj)        
    logger.debug("All Scrapped Items for "+""+websitename+""+url+""+" stored in Mongo Successfully...") 


def fetchingURLsList():
    listofLinksData=[]
    db=establishMongoConnection()
    posts = db.urls
    filteredJSON=posts.find({"Website" : "indiankanoon"})
    for jsonData in filteredJSON:
        listofLinksData.append(jsonData)
    return listofLinksData    

def fetchingIndianKanoonCasesFromDB():
    listofArticlesDetails=[]
    db=establishMongoConnection()
    logger.debug("Mongo Connection Established Successfully for Fetching all Articles Content from DB..")
    posts = db.cases
    cursor = posts.find()           
    for alldocs in cursor:
        listofArticlesDetails.append(alldocs)  
    return listofArticlesDetails

def storingNounsCorrespodingToCases(nounlist):
    NounsArray=nounlist
    db=establishMongoConnection()
    logger.debug("Mongo Connection Established Successfully for storing Nouns List in DB...")
    post=db.cases
    #print(NounsArray)
    
    for i in NounsArray:
        post.update_one({ "WebInfo.urlsInfo.urlName":i['urlName']
                                           },
                              {"$set":{ "WebInfo.urlsInfo.Nouns":i['NounsList'] }}
             )
    logger.debug("Nouns are stored in Mongo Successfully...")
    print("Stored..")
    
    return "success"    
        
def getKanoonData():
    listdata=[]
    db=establishMongoConnection()
    posts = db.cases
    cursor=posts.find({})
    for doc in cursor:
        listdata.append(doc)
    return listdata   

def storeTFofKanoonInDB(data):
    db=establishMongoConnection()
    posts = db.cases
    for doc in data:
        obj={
            
                                                   "tf":doc['tf'],
                                                    "flags":doc['flag'],
                                                   "terms":doc['terms'],
                                                   "tag" :"default"      
                        }
        posts.update({'WebInfo.urlsInfo.Title':doc['title']},
                            {'$set': {"WebInfo.urlsInfo.tfInfo":obj}},
                                 upsert=True
                                )
    print("Success")  

def storeTFIDF(data):
    db=establishMongoConnection()
    posts = db.cases
    for doc in data:
        obj={
                           "idf":doc['idf'],
                           "tfIdf":doc['tfIdf'],
                           "flags":doc['flag']
             }
        posts.update({'WebInfo.urlsInfo.Title':doc['title']},
                           {'$set': {"WebInfo.urlsInfo.tfInfo":obj}},
                           upsert=True
               
            )
    print("All IDF is Stored....") 

def getTFIDFFromDB():
    db=establishMongoConnection()
    posts = db.cases

    data = posts.find({},{"WebInfo.urlsInfo.tfInfo":1,"WebInfo.urlsInfo.Title":1})

    return data

def storeTopTFIDF(data):
    db=establishMongoConnection()
    posts = db.cases
    for doc in data:
        posts.update({'WebInfo.urlsInfo.Title':doc['Title']},
                           {'$set': {"WebInfo.urlsInfo.TopIdf":doc['TermsName']}},
                           upsert=True
               
             )
    print(" top IDf is stored..")

def updateTFIDF(data):
    db=establishMongoConnection()
    posts = db.cases
    
    for doc in data:
        posts.update({'WebInfo.urlsInfo.Title':doc['Title']}, { "$unset" : { "WebInfo.urlsInfo.tfInfo" : 1} })
        posts.update({'WebInfo.urlsInfo.Title':doc['Title']},
                           {'$set': {"WebInfo.urlsInfo.tfidf":doc['SortedTerms']}},
                           upsert=True
               
             )
    print(" IDf is stored..")          

#fetchingLastLevelsofUrlFromDB()    