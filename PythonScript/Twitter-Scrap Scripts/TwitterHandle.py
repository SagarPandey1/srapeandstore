from TwitterAuth import twitterPassport
from twitterDB import getTwitterHandle
from twitterDB import storeTwitterScrap
import re


def twitterHandle():
    TweetsObject=[]
    api = twitterPassport();
    handle=getTwitterHandle()
    for obj in handle:
        data=json_string = api.GetUserTimeline(screen_name=obj['handle'])
        tweets = [i.AsDict() for i in data]
        for t in tweets:
            TweetsObject.append({  
                                   "handle":obj['handle'],
                                   "ID":t['id'],
                                   "Tweet":t['text'],
                                   "Date_Created":t['created_at']

                    })
    return TweetsObject

def filterTwitterScrapAndStore():  
    array=twitterHandle()
    
    for text in array:
        filteredTweets = re.sub(r'(https|http)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', '', text['Tweet'], flags=re.MULTILINE)
        if(re.search("(https?://[^\s]+)", text['Tweet'])!=None):
            filteredLinks = re.search("(?P<url>https?://[^\s]+)", text['Tweet']).group("url")
            obj={
                             "handle":text['handle'],
                             "Tweets":filteredTweets,
                              "ID":text['ID'],
                              "Date_Created":text['Date_Created'],
                              "url":filteredLinks
                        }
            storeTwitterScrap(obj)            
        else:
            obj={
                             "handle":text['handle'],
                             "Tweets":filteredTweets,
                              "ID":text['ID'],
                              "Date_Created":text['Date_Created'],
                              "url":""
                        }
            storeTwitterScrap(obj)               

    return "Success"

filterTwitterScrapAndStore() 

       
   
    
    
    
