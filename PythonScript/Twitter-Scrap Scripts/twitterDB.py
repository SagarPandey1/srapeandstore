from SocialMedialogHandling import logSettings1
logger=logSettings1()

def establishMongoConnection():
   from pymongo import MongoClient
   client = MongoClient() 
   client = MongoClient('mongodb://localhost:27017')
   db = client['pythonNLP']                               
   return db

def getTwitterHandle():
    listofHandles=[]
    db=establishMongoConnection()
    logger.debug("Mongo Connection Established Successfully for Fetching all Twitter Handles from DB...")
    posts = db.twitterhandles
    cursor = posts.find()           
    for alldocs in cursor:
        listofHandles.append(alldocs) 
    return listofHandles


def storeTwitterScrap(object):
    db=establishMongoConnection()
    logger.debug("Mongo Connection Established Successfully for Storing Twitter Scrap in DB...")
    post = db.twitterdatas
    SavedUrls=post.insert_one({
             "TweetsData":object
        })    
    logger.debug("All TwitterData is Stored in DB Successfully...")
    return "success"


        

    
