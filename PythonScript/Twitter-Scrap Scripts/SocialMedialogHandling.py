import logging

def logSettings1():
    logger = logging.getLogger('SocialHandleLogs')
    hdlr = logging.FileHandler('/home/sagar/Desktop/Project/PythonLogs/SocialHandleLogs.log')
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr) 
    logger.setLevel(logging.DEBUG)
    return logger