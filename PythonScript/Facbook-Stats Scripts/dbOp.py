def establishMongoConnection():
    from pymongo import MongoClient
    client = MongoClient() 
    client = MongoClient('mongodb://localhost:27017')
    db = client['pythonNLP']                             
    return db


def fetchAnchors():
    listofArticlesDetails=[]
    db=establishMongoConnection()
    posts = db.updatedarticles
    cursor = posts.find({"WebInfo.urlsInfo.fbID":{"$exists":False}})         
    for alldocs in cursor:
        listofArticlesDetails.append(alldocs)      
    return listofArticlesDetails 


def storeLinksIDInDB(postID,urlName):
    db=establishMongoConnection()
    posts = db.updatedarticles
   
    posts.update_one({"WebInfo.urlsInfo.urlName":urlName},
                             {   "$set":{"WebInfo.urlsInfo.fbID":postID } },
                             upsert=True         
                                       )
    print("fb ID for url "+ urlName+" "+ "is stored in DB...")
    

def fetchFB_IDFromDB():
    listofLinksID=[]
    db=establishMongoConnection()
    posts = db.updatedarticles
    cursor = posts.find({"WebInfo.urlsInfo.fbID":{"$exists":True}},
                              {"WebInfo.urlsInfo.fbID":1,"WebInfo.urlsInfo.urlName":1,"WebSite":1})           
    for alldocs in cursor:
        listofLinksID.append(alldocs)       
    return listofLinksID 
   

def storeLinksStats1(statsInfo,fbID):                    #Store Links stats in Main Articles Collection
    db=establishMongoConnection()
    posts = db.updatedarticles

    posts.update({"WebInfo.urlsInfo.fbID":fbID},
                           {"$set" :{ "WebInfo.urlsInfo.fbStats":statsInfo } },
                           upsert=True
                )
    
    print("fb Stats Info is Stored in DB for ID "+ fbID)
    
    

def storeLinksStats2(statsInfo,fbID,siteName,url):           #Store Links stats in Main ArticleStatsHistories Collection
    db=establishMongoConnection()
    posts = db.articlesStatsHistories
    
    cursor=posts.find({"fbID":fbID})
    
    if(cursor.count()==0):
        posts.insert({
                        "WebSite" : siteName,
                        "fbID"    : fbID,
                        "url"     : url,
                        "statsInfo" : [statsInfo]
                    })
    
        print("fb stats Details are stored in DB for url "+url)

    else:
        posts.update({"fbID":fbID},
                             {"$push":{"statsInfo" : statsInfo} },
                             upsert=True            
                        )
        print("fb Stats Details are Updated & Stored in DB for url "+url)
            
   

    
    

  

