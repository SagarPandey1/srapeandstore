from facebookAuth import facebookPassport
from dbOp import fetchAnchors,storeLinksIDInDB
from dbOp import fetchFB_IDFromDB
from dbOp import storeLinksStats1
from dbOp import storeLinksStats2

import datetime
import facebook
now = datetime.datetime.now()
import time
import json
import urllib2

from urlparse import urlparse

# Header's Information....
user_agent = '5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36'

#-------------------- First get the ID for a URL from FB Open-Graph-----------------------------#

def getUrlsfromDb():
    links=fetchAnchors()
    for urls in links:
        try:
            if(urls['WebInfo']['urlsInfo']['fbID']):  # if id is already present in DB then it skip for find FB ID for that URL.
                continue
        except KeyError:          # if ID not present in DB then it genrate Key Exception Error that is Handled Here.
            anchor=urls['WebInfo']['urlsInfo']['urlName']
            if(anchor.startswith("/")):
                continue  
            path = urlparse(anchor).scheme+'://'+ urlparse(anchor).netloc + urlparse(anchor).path 
            print(path)  
            urlForOpenGraphCall(data=path,storedurl=anchor) 


def urlForOpenGraphCall(data,storedurl):
    accessTokenInfo = facebookPassport()
    token = accessTokenInfo['access_token']
    host = "https://graph.facebook.com/v2.12"
    path =  '/' + data 
    params = '?fields=og_object' + '&access_token='+ token 
    
    url = host + path + params
    #print(url)
    getArticlesIDFromFB(urlInfo=url,url=storedurl)

    return



def getArticlesIDFromFB(urlInfo,url):
    try:                                        #Exception handle when no object is Found....
        opener = urllib2.build_opener()
        opener.addheaders = [('User-Agent', user_agent)]
        response = opener.open(urlInfo)   
        JSONObject = response.read()
        obj =json.loads(JSONObject)                                
        fbID = obj['og_object']['id']
    except (urllib2.HTTPError , KeyError) as err:
        print(err)
        return    

    print(fbID)
    storeLinksIDInDB(postID=fbID,urlName=url)
    return
    




#------------------------- Second Fetch the Likes , Comments , Shares From FB by Using Stored FB_ID in DB --------#   


def getIDsFromDB():
    
    IDsArray = fetchFB_IDFromDB()
    for ids in IDsArray:
        fbID = ids['WebInfo']['urlsInfo']['fbID']
        websiteName = ids['WebSite']
        url = ids['WebInfo']['urlsInfo']['urlName']
        readyUrlBeforeOGCalls(postID=fbID,website=websiteName,urlName=url)


def readyUrlBeforeOGCalls(postID,website,urlName):
    accessTokenInfo = facebookPassport()
    token = accessTokenInfo['access_token']

    host = "https://graph.facebook.com/v2.12"
    path =  '/' + postID 
    params = '?fields=comments.summary(true),likes.summary(true),share.summary(true)' + '&access_token='+ token 
    
    url = host + path + params
    
    getLinksStats(fbID=postID,ogUrl=url,siteName=website,urlName=urlName)
    
    



def getLinksStats(fbID,ogUrl,siteName,urlName):
    try:
        opener = urllib2.build_opener()
        opener.addheaders = [('User-Agent', user_agent)]
        response = opener.open(ogUrl)   
        JSONObject = response.read()
        obj =json.loads(JSONObject)

    except urllib2.HTTPError:
        return    
    
    filteredStats = filterStatsInfo(obj)
    
    storeLinksStats1(statsInfo=filteredStats,fbID=fbID)      #Store Links stats in Main Articles Collection
    storeLinksStats2(statsInfo=filteredStats,fbID=fbID,siteName=siteName,url=urlName)      #Store Links stats in Main ArticleStatsHistories Collection


def filterStatsInfo(obj):
    TotalComments_Count = obj['comments']['summary']['total_count']
    TotalLikes_Count    = obj['likes']['summary']['total_count']
    ID                  = obj['id']

    finalStatsObject = {
                          "TotalComments" : TotalComments_Count,
                          "TotalLikes"    : TotalLikes_Count,
                          "time"          : now.strftime("%Y-%m-%d %H:%M")
                        }

    print(finalStatsObject)
    return finalStatsObject       
    

#getUrlsfromDb()
getIDsFromDB()